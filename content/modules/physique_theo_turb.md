Title: UE physique théorique de la turbulence 3ECTS (24h)
Date: 2021-01-20
Category:  Modules d'enseignement
lang: fr

Léonie Canet, Vincent Rossetto

![Image]({static}../figures/kolmogorov_spectrum.png)
_courtesy N.M. AJP publication_

#Résumé :

Les équations de Navier-Stokes restent à ce jour non résolues, et prédire les propriétés statistiques des écoulements turbulents représente un enjeu majeur pour de nombreuses applications. De nombreuses approches théoriques ont été développées pour d’une part modéliser, et d’autre part calculer le comportement des écoulements turbulents au-delà de la théorie de Kolmogorov de 1941. Ce cours présentera plusieurs de ces approches, depuis le formalisme multi-fractal jusqu’aux méthodes du groupe de renormalisation, en se concentrant sur le cas idéal de la turbulence homogène isotrope.

[pour plus de détails](https://cours.univ-grenoble-alpes.fr/course/view.php?id=20884)


#Répartition CM/TD:
environ 12 CM (18h) et 4 TD (6h)
Enseigné principalement en français

#Plan (indicatif) :

##PARTIE A — phénoménologie avancée de la turbulence

1. _Observations expérimentales et numériques de l’intermittence_:
manifestations de l’intermittence, propriétés universelles, principales déviations par rapport aux prédictions de la théorie de Kolmogorov 1941
2. _Le formalisme multi-fractal_:
rappel sur les fractales, invariance d'échelle, nécessité de rendre compte du caractère multi-échelle, théorie de Kolmogorov 62, modèles multi-fractaux


##PARTIE B —  description statistique de la turbulence
1. _Stochasticité spontanée_:
notion de stochasticité spontanée pour une équation différentielle ordinaire, stochasticité spontanée dans des modèles simplifiés de turbulence, justification d’approches statistiques
2. _Quelques modèles simplifiés de turbulence_:
Modèle de Lorenz et attracteur étrange, modèles en couches, équation de Burgers
3. _Le groupe de renormalisation_:
théorie de champs pour Navier-Stokes, symétries, Karman-Howarth à partir des symétries, groupe de renormalisation de Wilson.
