Title: Advanced experimental methods in turbulence 3 ECTS (24h)
Date: 2021-11-26
Category: Teaching modules
lang: en


# Summary :

3 Turbulence Practical Works in groups of 4,  applying the Turbulence and Process modules:
each 3h lab and 3h data analysis.
1 Turbulence Practical work in the frame of a research experimental project.

# Distribution CM/TD:

about 3h CM, 3\*3h TP, 4\*3h TD


# Content (indicative) :

* TP 1 [ Aerodynamic wind tunnel : ]({filename}TP/sillage.md)   turbulent wake

* TP 2a [active grid wind tunnel  : ]({filename}TP/thi.md) Isotropic Homogeneous Turbulence

* TP 2b data [Modane grid wind tunnel : ]({filename}TP/thi_modane.md)  Turbulence with very high Reynolds number (2024)

* TP 3 [turbulent axisymetric jet wind tunnel : ]({filename}TP/jet.md)  turbulent jet (2024)

* TP 4a visit [Coriolis LEGI rotating plate  : ]({filename}TP/Coriolis.md) stratified turbulence and rotation

* TP 4b ISTERRE rotating plate (2024)

* TP 5 visit [wave and tild channel  LEGI : ]({filename}TP/houle.md) wave turbulence

* TP 6 [vibrated plates/gong : ]({filename}TP/plaque.md) wave turbulence

* TP 7 [variable slope channel  : ]({filename}TP/pente.md) rough turbulent boundary layer (2024)

* TP 8 [stratified gravity flow channel : ]({filename}TP/gravitaire.md) stratified turbulence along a slope

* TP 9 [mountain atmospheric boundary layer : ]({filename}TP/cla.md) turbulent atmospheric bundary layer

* TP 10 visit [mixing layer with cavitation : ]({filename}TP/cavitation.md) compressible turbulence

* TP 11 [bubble column : ]({filename}TP/colonne_bulle.md)  two-phase turbulence

* TP 12 [lagrangian flow : ]({filename}TP/lagrangien.md) two-phase turbulence

* TP 13 visit [jet atomization : ]({filename}TP/atomisation.md)  two-phase turbulence

* TP 14 visit  Helium turbulence facility :  superfluid Turbulence (2024)




