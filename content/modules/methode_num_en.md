Title: Advanced Numerical Methods 3ECTS (24h)
Date: 2021-01-25
Category:  Teaching modules
lang: en

Christophe Picard , Guillaume Balarac, Pierre Augier

![Image]({static}../figures/KH_goertler.png)
_courtesy J.D. PhD Thesis_

# Abstract:

Navier Stokes – HPC

# Number of sessions:

About 14 courses/practical sessions (21h)

# Plan:

## Part I - Reminder of numerical methods and notion of HPC  (Christophe Picard)

Course / Concepts covered:

- Numerical methods for Navier-Stokes: finite differences, finite volumes,
Smoothed-Particle Hydrodynamics, Lattice Boltzmann, etc... coupling
Pressure-Velocity (Poisson equation)

- High Performance Computing : notion of parallelism

TP :

- Finite difference solver for 2D Navier-Stokes, application to Taylor-Green Vortex flow

## Part II: Spectral approach and 2D turbulence (Pierre Augier)

Course / Concepts covered:

- Notion of 2D turbulence: scale developments in Turbulence
- Spectral approach
- Version management : Gitlab

Practical work:

- FluidSim in 2D Turbulence: energy transfer and flow concept

### Part III : 3D turbulence and turbulence modeling (Guillaume Balarac)

Lecture / Concepts covered:

- Reminder of 3D turbulence
- Turbulence modeling: DNS, LES, RANS

Practical work:

- FluidSim in 3D Turbulence: energy transfer and LES modeling; energy balance
