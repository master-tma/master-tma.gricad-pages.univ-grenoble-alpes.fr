Title: Compressible turbulence 3ECTS (24h)
Date: 2021-01-25
Category:  Teaching modules
lang: en

Christophe Brun, Stéphane Barre, Pierre Hily-Blant

![Image]({static}../figures/choc_melange.png)
_courtesy C.B. TCFD publication_

#Abstract :
Training in the basics of compressible turbulence including astrophysics medium.
#Distribution CM/TD:
approximately 4 CM (6h), 4 TD (6h), 6 CM/TD (9h) and 1 TP (3h).
#Content :
##Part 1: Compressible turbulence in astrophysics (Lecture slides and notes are in english, but oral presentation is in french)
1. Compressible turbulence in the interstellar medium (ISM):
   * Role of turbulence in star formation
   * Feedback of star formation on galaxy evolution
   * Toward the gravo-turbulence framework

2. The ISM: a very brief introduction
   * Composition
   * ISM phases: thermodynamics of the ISM and the bistable equation of state
   * Pieces of evidence of interstellar turbulence
     - Pressure: thermal, magnetic, cosmic-rays, non-thermal
     - Scaling laws (in ionized and neutral medium)
     - Reynolds number
     - What is ISM turbulence?

3. Observation of ISM turbulence
   * Power spectrum
   * Structure function analysis and intermittency

4. Theory and numerical simulations
   * Modifications to K41 for compressible turbulence
   * Comparison between compressible and incompressible
     - Power spectrum
     - Structure functions and (multi-)fractal models
   * Magnetized turbulence: challenges
   * Predictions from numerical simulations

5. Open questions in ISM turbulence

##Part 2: Compressible turbulence in aerodynamics
6. transonic compressible fluid mechanics
    * Kovasznay entropic, acoustic, vortical waves. Morkovin hypothesis Non-hypersonic turbulence
    * compressible Navier-Stokes equations - adimmension of Reynolds and Mach numbers
    * compressible numerical methods: conservative schemes (Mac Cormak, shock capture...), non-reflective boundary conditions,
    * oblique shocks - Rankine Hugoniot relations
    * compressible turbulence - dynamic and thermal boundary layer: SRA Reynolds analogy, Crocco Busemann relations
    * Numerical experiments: shock tube, turbulent compressible plane channel, turbulent compressible plane jet

7. compressible turbulence and instrumentation
    * nozzle, oblique shock type air intake, shock tube,...
    * Experimental demonstration of linearized modes of compressible turbulence (Kovasznay) by using the hot wire and fluctuation diagrams.
