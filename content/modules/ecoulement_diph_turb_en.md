title: Two phase turbulent flows 3ECTS
date: 2021-01-22
Category: Teaching modules
lang: en

M. Obligado, P. sechet, G. Balarac, N. Machicoane

![Image]({static}../figures/ombro.png)

[Ombroscopie: Freely advected ice ball in a turbulent flow (voir vidéo).](https://www.youtube.com/watch?v=Rmt4CtiDOys)
_courtesy N.M._

#Abstract :
Local instantaneous conservation equations at interfaces and on a triple line.
Behaviour and state laws at interfaces: general concepts on contaminated interfaces (surfactants and surface tension, adsorption isotherm, adsorption kinetics, interfacial molecular transport) and notions on surface rheology.
Measurement of surface tension. Measurement of surface viscosities by capillary wave attenuation. Characterisation of adsorption-desorption kinetics from the Savart experiment.
Terminal velocity of inclusions in an infinite stagnant medium.
BBOT equation.
Particles in turbulence and atomisation.

#Distribution CM/TD:
About 12 CM/TD (18h) and 2 TP (6h)

#Content (indicative) :

##PART A - interface hydrodynamic instability
1. Introduce a mechanical description of interfaces. From reminders of the conservation equations (mass and momentuḿ) within a phase, the mass and momentuḿ balances at interfaces are established by analogy. We then concern ourselves with state and constitutive laws intended to provide information on the behaviour of materials at the interface between two phases. Finally, it appears that the surface tension depends on the temperature but also on the presence of surface-active agents (or surfactants) which it is generally difficult to avoid in the majoritý of applications. We are therefore led to equip ourselves with the necessary tools to model the transport of surfactant molecules. Finally, one is interested in the means of measuring the introduced variables.
2. Instabilities at the interface of two fluids and fragmentation: Kelvin Helmholtz (KH), Rayleigh-Taylor (RT) and fragmentation (bubbles, drops, sprays). Weber number, relative velocity.
3. (under construction)


##PART B - Description of turbulent multiphase flows
(phase distribution, inclusion sizes, fluctuations) or effect of particle size and density on transport

1. MO: We are interested in particles whose size is sufficient to escape Brownian motion (typically ). Similarly, effects such as thermophoresis, electrophoresis, etc., which occur for small particles (typically ) will not be considered.
This part includes: motion in an infinite medium of isolated inclusions: weak and strong permanent case (drag - deformation - instabilitý of trajectory - influence of contaminants on drag) and non-permanent (added mass, history term) / general equation of a small globule in any fluid field (BBOT equation) / response of small particles in turbulent carrier field/Overview of the lift forces.
2. Role of density and size on the transport of large particles (Maxey Riley).
3. PS: pseudo turbulence in a bubble column
Introduction: generalities, what is induced agitation, effects on phase structuring in bubble columns, examples from observations
- Definitions :
    - unconditional fields
    - conditional fields and fluctuating quantities
    - pair density and structure function
- Putting into equation (hybrid formulation)
    - Unconditional field equations
    - Conditional field equations
- Example: the simplified 2-body problem
	- closure of equations for the simplified 2-body problem
   	- mechanism of interactions between bubbles deduced from the simplified problem and the scaling of fluctuations
   	- scaling law and comparison with experimental results
- 3 experimental tests: measurement in a bubble column with an optical probe, Lagrangian tracking of an isolated bubble and deoxygenation of water.

##PART C - two-phase mass and heat transfer
1. Heat transfer in turbulent convection - Experimental approach to the melting of large ice balls. Applications in process engineering and heat exchangers. For small objects, discussion of bubble dissolution and droplet evaporation in turbulence.
2. (under construction)
3. (under construction)

##PART D - two-phase turbulence in LES and RANS (under construction)
