Title: Turbulent atmospheric boundary layer 3ECTS (24h)
Date: 2021-11-25
Category:  Teaching modules
lang: en

Christophe Brun, Jean-Martial Cohard

![Image]({static}../figures/ballon.png)
_courtesy C.B. BLM publication_

#Abstract :

Training in the basics of turbulence in the atmospheric boundary layer.


#Distribution CM/TD:
About 6 CM (9h), 8 Seminars (12h) and 1 TP (3h).

#Content :

##Part 1: Turbulence in the atmospheric boundary layer (under construction)

1. Turbulence
    * turbulent kinetic energy balance TKE and turbulent potential energy balance TPE
    * balance of momentum flux and sensible heat flux
    * anisotropy of turbulence
    * energy spectra

2. Neutral, stable and unstable CLA
    * thermal stratification
    * buoyancy and mechanical shear balance
    * Obukhov length scale
    * correction of the logarithmic law of velocity and temperature

## Part 2 : Seminars on Turbulence in the atmosphere

1. F. Cantalloube & J. Milli :  Imagerie à travers la turbulence en astronomie; Correction par optique adaptative 

2. H. Jacquet : Atmosphere response to a submesoscale SST front

3. J.M. Cohard : la turbulence et la scintillométrie : impacts sur la propagation d'onde electromagnétique

4. M. Barthelemy : la Turbulence dans l’ionosphère et dans le couplage Ionosphère-magnétosphère

5. P. Augier : la turbulence d'onde dans la troposphère 

6. A. Wirth : turbulent transport in the turbulent atmopheric boundary layer

7. J. Peinke : A general joint multipoint approach to turbulence and its thermodynamical interpretation - Reflection on a new definition of turbulent structures

8. J.E. Sicart : la turbulence dans la couche limite atmosphérique au-dessus des glaciers

9. C. Brun : la turbulence dans les vents catabatiques sur les pentes alpines (2025)

##Part 3 : Optic Turbulence  (under construction for 2025)
1. Electromagnetic wave propagation in random media
    * Structure parameter in the Atmospheric Boundary Layer (definition, similarity theory, ...)
    * Formulation of electromagnetic wave propagation in random media
    * impacts of turbulence on signal strength and angle of arrival
    * Heat and water transfers from surface to atmosphere.
    * Instrumentation and application
