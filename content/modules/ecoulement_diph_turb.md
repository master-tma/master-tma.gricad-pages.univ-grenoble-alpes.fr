title: UE Ecoulements diphasiques turbulents 3ECTS
date: 2021-01-22
Category:  Modules d'enseignement
lang: fr

M. Obligado, P. sechet, G. Balarac, N. Machicoane

![Image]({static}../figures/ombro.png)

[Ombroscopie: Freely advected ice ball in a turbulent flow (voir vidéo).](https://www.youtube.com/watch?v=Rmt4CtiDOys)
_courtesy N.M._

#Résumé :
Équations de conservation locales instantanées aux interfaces et sur une ligne triple.
Lois de comportement et lois d’état aux interfaces : concepts généraux sur les interfaces contaminées (surfactants et tension superficielle, isotherme d’adsorption, cinétique d’adsorption, transport moléculaire interfacial) et notions de rhéologie de surface.
Mesure de la tension superficielle. Mesure des viscosités de surface par atténuation d’ondes capillaires. Caractérisation de la cinétique d’adsorption-désorption à partir de l’expérience de Savart.
Vitesse terminale des inclusions en milieu infini stagnant.
Équation BBOT.
Particules en turbulence et atomisation.

#Répartition CM/TD:
environ 12 CM/TD (18h) et 2 TP (6h)

#Plan (indicatif) :

##PARTIE A — instabilité hydrodynamique d'interface
1. Introduire une description des interfaces de type mécanique. À partir de rappels sur les équations de conservation (masse et quantité́ de mouvement) au sein d’une phase, on établit par analogie les bilans de masse et de quantité́ de mouvement aux interfaces. On se préoccupe ensuite des lois d’état et des lois constitutives destinées à renseigner sur le comportement des matériaux à l’interface entre deux phases. Enfin, il apparaît que la tension de surface dépend de la température mais aussi de la présence d’agents tensioactifs (ou surfactants) qu’il est généralement difficile d’éviter dans la majorité́ des applications. On est donc conduit à se doter des outils nécessaires pour modéliser le transport de molécules de surfactants. On s’intéresse enfin aux moyens de mesurer les variables introduites.
2. Instabilités à l'interface de deux fluides et fragmentation: Kelvin Helmholtz (KH), Rayleigh-Taylor (RT) et fragmentation (bulles, gouttes, sprays). Nombre de Weber, vitesse relatives.
3. (en construction)


##PARTIE B — Description des écoulements multiphasiques turbulent
(distribution des phases, tailles des inclusions, fluctuations) ou effet de la taille et de la densités des particules sur leur transport

1. MO : On s’intéresse à des particules dont la taille est suffisante pour échapper au mouvement Brownien (typiquement ). De même, les effets du type thermophorèse, électrophorèse etc… qui interviennent pour des particules de faibles dimensions (typiquement ) ne seront pas considérés.
Cette partie comprend : le mouvement en milieu infini d’inclusions isolées : cas permanent à faible et à fort  (traînée - déformation - instabilité́ de trajectoire - influence de contaminants sur la traînée) et non permanent (masse ajoutée, terme d’histoire) / équation générale d’un petit globule dans un champ fluide quelconque (équation de BBOT) / réponse de petites particules en champ porteur turbulent/Aperçu sur les forces de portances.
2. Rôle de la densité et de la taille sur le transport de grosses particules (Maxey Riley).
3. PS : pseudo turbulence en colonne à bulle
Introduction : généralités, qu'est ce que l'agitation induites, effets sur la structuration des phases en colonne à bulles, exemples issus des observations
- Definitions :
    - champs inconditionnels
    - champs conditionnels et grandeur fluctuantes
    - densité de paire et fonction de structure
- Mise en équation (formulation hybride)
    - Equations du champs inconditionnel
    - Equations du champs conditionnel
- Exemple : le problème à 2 corps simplifié
	- fermeture des equations pour le problème à 2 corps simplifié
   	- mécanisme d'interactions entre bulles déduite du problème simplifié et écrantage des fluctuations
   	- loi d'échelles et comparaison avec les résultats expérimentaux
- 3 TPs expérimentaux : mesure dans une colonne à bulles avec une sonde optique, suivi lagrangien d’une bulle isolée et désoxygénation de l’eau.

##PARTIE C — transfert de masse et de chaleur diphasique
1. Transfert de chaleur en convection turbulente - Approche expérimentale de la fusion de grosses billes de glace. Applications en génie des procédés et échangeurs thermiques. Pour les petits objets, discussion de la dissolution de bulles et de l'évaporation de gouttelettes en turbulence.
2. (en construction)
3. (en construction)

##PARTIE D — turbulence diphasique en LES et RANS (en construction)
