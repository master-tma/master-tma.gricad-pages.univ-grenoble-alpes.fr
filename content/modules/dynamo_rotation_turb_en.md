Title: Dynamo effect and rotation in turbulence 3ECTS (21h)
Date: 2021-01-25
Category:  Teaching modules
lang: en

Renaud Deguen, Nathanael Schaeffer, David Cebron,

![Image]({static}../figures/U_3D_intensity_hires.jpg)

[Simulation of the Magnetic field within the Earth's core (voir vidéo).](https://figshare.com/articles/media/Simulation_of_the_Magnetic_field_within_the_Earth_s_core/5976943)
_courtesy N.S. GJI publication_

#Abstract :

This course provides an introduction to turbulence in the fluid layers of planetary interiors (metal cores and magnetic field generation through turbulence -- dynamo effect --, gaseous planets, dynamics of subsurface oceans in ice satellites).

#Distribution CM/TD:
approximately 9 CM (13.5h), 9 TD (13.5h) and 1 TP (3h). Taught in english or/and french

#Content :
After a general discussion of the internal structure of planets and key observations (in particular the temporal variability of the magnetic field, indicative of non-linear dynamics in the Earth's core), the following topics will be addressed:

##Part 1: Rapidly rotating turbulence in a spherical shell.
Inertial waves, gravity and Rossby waves, natural convection, precession and tides, inverse cascade. Applications to the Earth's core,  gas giants, and subsurface oceans of the icy satellites of Jupiter and Saturn.

##Part 2: Generation of a planetary magnetic field by dynamo effect.
Induction equation and principle of the dynamo effect, kinematic dynamos, turbulent dynamos, alpha and omega effects, Alfven waves, numerical models of planetary dynamos...
