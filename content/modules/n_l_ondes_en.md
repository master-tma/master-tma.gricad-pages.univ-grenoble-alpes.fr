Title: Wave turbulence and non-linearities 3ECTS (24h)
Date: 2021-11-25
Category:  Teaching modules
lang: en

Nicolas Mordant, Pierre Augier, Mathieu Gibert

![Image]({static}../films/vagues3nosound.mp4)
_courtesy N.M._

#Summary: (under construction)


#Distribution CM/TD:
approximately 10 CM/TD (15h) and 1 TP (9h)
Taught mainly in French

#Content :
##Part 1: generalities
nonlinear interactions, triads/quartetts and co.

##Part 2 : wave turbulence
 introduction to the phenomenology of weak turbulence, dimensional analysis, examples of application, limitations, light version of the theory (version of the book by Falkovich, L'vov, Zakharov rather than Newell's developments in cumulants)

##Part 3 introduction to solitons and integrable turbulence/soliton gas

##Part 4: introduction to geophysical turbulence
internal gravity waves, inertial waves, inertial-gravity waves, phenomenology of stratified and/or rapidly rotating turbulence
vortex waves in the He superfluid

## TP:
* Lab work on wave turbulence e.g. on vibrated plates/gongs, waves at the water surface (solitons).
* TP on the Coriolis teaching platform on rotating flows
* Numerical exercises
