Title: UE Contrôle et Turbulence de paroi 3ECTS (24h)
Date: 2021-01-25
Category: Modules d'enseignement
lang: fr

Sedat Tardu

![Image]({static}../figures/requin.png)
_courtesy S.T. Wall Turbulence Control publication_

# Résumé : (en construction)

# Répartition CM/TD: (en construction)

# Plan :

## Partie 1 : Turbulence de paroi :

Equations de Transport des contraintes de Reynolds, turbulence homogène, isotrope, équations exactes dans un écoulement turbulent en canal 2D; équations de Re couche limite turbulente 2D; Fermetures en un point, sous-couche logarithmique, visqueuse et tampon; distributions des contraintes, production dissipation.

## Partie 2 : Contrôle des écoulements (trainée) :

Contrôle passif (LEBS's, Riblets, "complied walls"; contrôle actif soufflage instationnaire; contrôle non-linéaire optimal et suboptimal; contrôle dual; contrôle de la séparation

## Partie 3 : Turbulence Instationnaire forcée :

Réponse de la turbulence pariétale à des excitations forcées périodiques de vitesse; effet sur l'écoulement moyen, modulations des quantités fluctuantes; réponse des structures cohérentes et des stries; réponse de la turbulence pariétale à une accélération de la vitesse débitante.
