Title: UE Effet dynamo et rotation en turbulence 3ECTS (21h)
Date: 2021-01-25
Category:  Modules d'enseignement
lang: fr

Renaud Deguen, Nathanael Schaeffer, David Cebron,

![Image]({static}../figures/U_3D_intensity_hires.jpg)

[Simulation of the Magnetic field within the Earth's core (voir vidéo).](https://figshare.com/articles/media/Simulation_of_the_Magnetic_field_within_the_Earth_s_core/5976943)
_courtesy N.S. GJI publication_

#Résumé :

Cette UE propose une introduction à la turbulence des couches fluides des intérieurs planétaires (noyaux métalliques et génération de champ magnétique par la turbulence -- effet dynamo --, planètes gazeuses, dynamique des océans de sub-surface dans les satellites de glace).

#Répartition CM/TD:
environ 9 CM (13.5h), 9 TD (13.5h) et 1 TP (3h)

#Plan :
Après une discussion générale de la structure interne des planètes et des observations clés (en particulier la variabilité temporelle du champ magnétique, indicatrice d'une dynamique non-linéaire dans le noyau terrestre), on abordera les thèmes suivant:

##Partie 1 : turbulence en rotation rapide dans une coquille sphérique.
Ondes inertielles, de gravité, rosby, convection naturelle, effets de précession et marées, cascade inverse. Applications au noyau terrestre, aux planètes gazeuses, et aux océans de sub-surface des satellites de glace de Jupiter et Saturne.

##Partie 2 :  génération d'un champ magnétique planétaire par effet dynamo.
Equation de l'induction et principe de l'effet dynamo, dynamos cinématiques, dynamos turbulentes, effets alpha et omega, ondes d'Alfven, modèles numériques de dynamos planétaires...
