Title: UE Méthodes Numériques avancées 3ECTS (24h)
Date: 2021-01-25
Category:  Modules d'enseignement
lang: fr

Christophe Picard , Guillaume Balarac, Pierre Augier

![Image]({static}../figures/KH_goertler.png)
_courtesy J.D. PhD Thesis_

# Résumé :

Navier Stokes – HPC

# Nombre de séances:

about 14 CM/TD (21h)

# Plan :

## Partie I - Rappel de méthodes numérique et notion de HPC (Christophe Picard)

Cours / Notions abordées :

- Méthodes numériques pour Navier-Stokes : différences finies, volumes finies,
Smoothed-Particle Hydrodynamics, Lattice Boltzmann, etc... couplage
Pression-Vitesse (équation de Poisson)

- High Performance Computing : notion de parallélisme

TP :

- solver différences finies pour Navier-Stokes 2D, application à l'écoulement de Taylor-Green Vortex

## Partie II : Approche spectrale et Turbulence 2D (Pierre Augier)

Cours / Notions abordées :

- Notion de turbulence 2D : développements d'échelles en Turbulence
- Approche spectrale
- Gestion de version : Gitlab

TP :

- FluidSim en Turbulence 2D : transfert d'énergie et notion de flux

### Partie III : Turbulence 3D et modélisation de la turbulence (Guillaume Balarac)

Cours / Notions abordées :

- Rappel de turbulence 3D
- Modélisation de la turbulence : DNS, LES, RANS

TP :

- FluidSim en Turbulence 3D : transfert d'énergie et modélisation LES ; équilibre énergétique
