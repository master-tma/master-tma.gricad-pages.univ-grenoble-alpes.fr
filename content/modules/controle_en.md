Title: Control and wall turbulence 3ECTS (24h)
Date: 2021-11-25
Category: Teaching modules
lang: en

Sedat Tardu

![Image]({static}../figures/requin.png)
_courtesy S.T. Wall Turbulence Control publication_

#Summary: (under construction)


#Cotent CM/TD: (under construction)


#Plan :
##Part 1: Wall Turbulence:

Reynolds Stress Transport equations, homogeneous, isotropic turbulence, exact equations in 2D turbulent channel flow; 2D turbulent boundary layer equations; One-point closures, logarithmic, viscous and buffer sublayer; stress distributions, production dissipation.

##Part 2: Flow control (drag):
Passive control (LEBS's, Riblets, "complied walls"; active control unsteady blowing; optimal and suboptimal nonlinear control; dual control; separation control

##Part 3: Forced Unsteady Turbulence:
Response of parietal turbulence to periodic forced velocity excitations; effect on mean flow, modulations of fluctuating quantities; response of coherent structures and streaks; response of parietal turbulence to accelerated flow velocity.
