Title: TP Couche limite atmosphérique turbulente
Date: 2022-02-13
Category: TP Turbulence
lang: fr, en


Ce TP a pour objectif de :

* prendre en main un système expérimental de mesure in situ de couche limite atmosphérique (CLA) et la logistique associée.

* effectuer des profils verticaux de mesure avec chacun des instruments (station météorologique, sonde instrumentée sous ballon).

* effectuer des mesures météorologiques classiques (Température, humidité, pression, vitesse à 2m, bilan radiatif au sol) pour reconstruire le cycle diurne sur la semaine.

* effectuer des mesures de turbulence 3D à haute fréquence (anémomètre sonique).

* caractériser les variables d’intérêt pour la CLA.

* analyser les profils et les remettre dans le contexte d’une couche limite turbulente stable hivernale (ou pas).

![Image]({static}../../figures/ballon.png)
