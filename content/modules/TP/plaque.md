Title: TP turbulence d'ondes
Date: 2022-02-13
Category: TP Turbulence 
lang: fr, en

* Titre: Turbulence d'ondes sur une plaque vibrée

* Objectifs: On s'intéressera à la turbulence d'ondes de flexion se propageant dans une plaque mince vibrée ou un gong. On caractérisera les propriétés statistiques par des mesures simples acoustiques du bruit émis puis une mesure directe de la vibration sera effectuée au moyen d'une technique de profilométrie par imagerie rapide. Cela permettra de mettre en évidence le caractère faiblement non linéaire de la turbulence d'onde ainsi que la cascade d'énergie en échelle.

![Image]({attach}../../figures/Tp_gong.png)
