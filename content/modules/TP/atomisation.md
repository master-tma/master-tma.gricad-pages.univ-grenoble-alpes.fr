Title: TP démonstration atomisation de jet
Date: 2022-02-13
Category: TP Turbulence 
lang: fr, en

* title: Destabilization of a liquid jet by a surrounding gas jet

* context: 

Gas-assisted atomization is a widely used process used in industry (manufacturing, pharmaceutical, agricultural…) to produce a high-quality spray. A high-speed gas accelerates and destabilizes a low-momentum liquid jet, to successively break it into a cloud of fine droplets, namely a spray. In the context of propulsion application, the gas enters the atomizer goes through the jet engine turbines and becomes highly turbulent, with large velocity fluctuations as well as angular momentum, referred to as swirl. Despite their widespread use in industry, the modeling of such coaxial atomizer remains unsatisfactory due to the lack of a complete mechanistic understanding of the underlying physical phenomena describing the interfacial instabilities and the break-up processes. The design and exploitation of these systems rely on empirical approaches, extracted on incomplete parameter range with respect to the wide nature of the applications. These open scientific questions and industrial challenges makes it an active field of research.

* abstract: 

We study the destabilization of a round liquid jet by an annular gas jet surrounding it. The liquid jet is laminar while the gas jet is turbulent. Instabilities are observed on the gas-liquid interfaces and lead to the break-up of the jet. We will use image analysis to find the boundaries of the liquid jet and measure the location of the break-up location on each snapshot. This will be followed by signal processing steps to study the statistics and temporal dynamic of the break-up location.

![Image]({attach}../../figures/TP_atomization.png)
