Title: TP Turbulence Homogène Isotrope
Date: 2022-02-13
Category: TP Turbulence
lang: fr, en

* Title: Grid-generated homogeneous isotropic turbulence

* Context

Passive and active grids are widespread for the generation of turbulent
flows in wind tunnels. They are not only easy to manufacture but they
also generate almost ideal homogeneous isotropic turbulence with a
very slow decay. They are therefore an excellent tool to study many
predictions from Kolmogorov's phenomenology

* Objectives

Within this lab session the students will use hot-wire anemometry to
test different grids, both passive and active. The high spatiotemporal
resolution of hot-wire signals will allow to extract relevant turbulence
parameters (Reynolds number, turbulent kinetic energy dissipation
rate, Kolmogorov scales...) and use them to verify the validity of
Kolmogorov phenomenology predictions.

* Keywords

Homogeneous isotropic turbulence, active grids, Kolmogorov
phenomenology

![Image]({attach}../../figures/activ_grid.png)
