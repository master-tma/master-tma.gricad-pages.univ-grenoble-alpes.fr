Title: TP turbulence stratifiée en rotation
Date: 2022-02-13
Category: TP Turbulence
lang: fr, en

The Coriolis platform is a unic infrastructure to model experimentally atmospheric and oceanic flows, which paly a key role for the planet climat. Its large dimensions allow to minimize the influence of viscosity, thus reproducing turbulent flows very close to those known in nature. The effect of stratification and rotation, the main driving focrces of geophyscal flows, can be taken into account, along with complex topographies. As a cutting-edge European facility, the Coriolis platform regularly hosts researchers both nationally and internationally. Indeed, the platform is not used internally by the LEGI researcher, but is also involved in european projects as one of the suitable infrastructures for the study of turbulence. Typical projects vary from the study of internal gravity waves, gravity currents (Mediterranean outflow at Gibraltar strait), western boundary currents (e.g. gulf stream), exchange flows and plunging plumes (e.g. estuaries), Rossby waves on beta-planes, turbulent jets (e.g. industry discharges into rivers) and many others. 
The experimental techniques are based on optical non intrusive techniques (PIV, LIF, PTV) and on local measurements using acoustic dopplers (ADV), conductivity probes for the density measurements and bed scans to reconstruct the bed morphology in case of bottom sediments.
In dependence on the actual research running on the Coriolis platform, the project will participate to the project running at that time. 

An example of a project run on the Coriolis Platform is given below.
New theoretical analyses and numerical computations have shown that regions of the continental shelf where the offshore shelf profile changes significantly can be sources of coastal eddies and thus regions of coastal turbulence. The size and intensity of the eddies is determined by the rapidity of the change in the profile and also by the stratification of the flow on the shelf. Eddies can be excited both by coastal currents passing along the shelf over the profile change and, in the correct parameter regime, by incident shelf waves. A series of experiments has be carried out on the Coriolis platform using PIV and ultrasonic probes to demonstrate this mechanism for coastal eddy generation in two-layer flow. The results will be compared with existing and new theory and with new numerical simulations, both in the quasi-geostrophic limit and in the MIT-GCM.

![Image]({attach}../../figures/coriolis_plateforme2.png)

