Title: UE data assimilation in geosciences 3ECTS (24h)
Date: 2021-08-25
Category:  Modules d'enseignement
lang: fr, en

Emmanuel Cosmes

# Résumé :

Le cours introduit des bases théoriques et méthodologiques pour la résolution de problèmes d’assimilation de données (inversion de données distribuées dans le temps) en sciences de la Terre. Les méthodes principalement abordées sont le filtre de Kalman (canonique et d'ensemble), le filtre particulaire, et le 4DVar. Il est produit sous forme de cours magistraux interactifs incluant la présentation et la manipulation d’exercices sous forme numérique en python. En fin de cours, les étudiant·es sont en capacité de :

- poser un problème d'assimilation de données simple et mettre en oeuvre une méthode de résolution ;

- communiquer avec des experts d'assimilation pour progresser vers la résolution de problèmes complexes.

Pré-requis : Notions classiques d’algèbre linéaire, de calcul différentiel et de statistiques.

Langue : français ou anglais 

# Abstract:

The course introduces theoretical and methodological foundations for solving data assimilation problems (inversion of time-distributed data) in the Earth sciences. The main methods covered are the Kalman filter (canonical and ensemble), the particle filter, and the 4DVar. It is produced in the form of interactive lectures including the presentation and manipulation of exercises in numerical form in Python. At the end of the course, students will be able to :

- pose a simple data assimilation problem and implement a solution method

- communicate with data assimilation experts to progress towards solving complex problems.

Prerequisites: Classical notions of linear algebra, differential calculus and statistics.

Language: French or English 
