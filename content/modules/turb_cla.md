Title: UE Turbulence en couche limite atmosphérique 3ECTS (24h)
Date: 2021-11-25
Category:  Modules d'enseignement
lang: fr

Christophe Brun, Jean-Martial Cohard

![Image]({static}../figures/ballon.png)
_courtesy C.B. BLM publication_

# Résumé :

former aux bases de la turbulence dans la couche limite atmosphérique.

# Répartition CM/TD:

environ 6 CM (9h), 8 Séminaires (12h) et 1 TP (3h).

# Plan :

## Partie 1 : Turbulence en couche limite atmosphérique

1. Turbulence
    * bilan d'énergie cinétique turbulente TKE et d'énergie potentielle turbulente TPE
    * bilan de flux de quantité de mouvement et de flux de chaleur sensible
    * anisotropie de la turbulence
    * spectres d'énergie

2. CLA neutre, stable et instable
    * stratification thermique
    * équilibre flottabilité et cisaillement mécanique
    * échelle de longueur d'Obukhov
    * correction de la loi logarithmique de vitesse et de température

## Partie 2 : Séminaires sur la Turbulence dans l'atmosphère

1. F. Cantalloube & J. Milli :  Imagerie à travers la turbulence en astronomie; Correction par optique adaptative 

2. H. Jacquet : Atmosphere response to a submesoscale SST front

3. J.M. Cohard : la turbulence et la scintillométrie : impacts sur la propagation d'onde electromagnétique

4. M. Barthelemy : la Turbulence dans l’ionosphère et dans le couplage Ionosphère-magnétosphère

5. P. Augier : la turbulence d'onde dans la troposphère 

6. A. Wirth : turbulent transport in the turbulent atmopheric boundary layer

7. J. Peinke : A general joint multipoint approach to turbulence and its thermodynamical interpretation - Reflection on a new definition of turbulent structures

8. J.E. Sicart : la turbulence dans la couche limite atmosphérique au-dessus des glaciers

9. C. Brun : la turbulence dans les vents catabatiques sur les pentes alpines (2025)
    
##Partie 3 : Turbulence optique (en construction pour 2025)

1. Propagation des ondes électromagnétiques dans les milieux aléatoires
    * Paramètre de structure dans la couche limite atmosphérique (définition, théorie de la similarité, ...)
    * Formulation de la propagation des ondes électromagnétiques dans les milieux aléatoires.
    * Impacts de la turbulence sur la force du signal et l'angle d'arrivée.
    * Transferts de chaleur et d'eau de la surface à l'atmosphère.
    * Instrumentation et application
