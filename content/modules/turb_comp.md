Title: UE Turbulence compressible 3ECTS (24h)
Date: 2021-01-25
Category:  Modules d'enseignement
lang: fr

Christophe Brun, Stéphane Barre, Pierre Hily-Blant

![Image]({static}../figures/choc_melange.png)
_courtesy C.B. TCFD publication_

#Résumé :

former aux bases de la turbulence compressible, y compris en milieu astrophysique.


#Répartition CM/TD:
environ 4 CM (6h), 4 TD (6h), 6 CM/TD (9h) et 1 TP (3h)

#Plan :
##Partie 1 : Turbulence compressible en astrophysique
1. description des chocs et de la turbulence :
    * milieu interstellaire auto-gravitant 
    * formation d'étoiles
    * observation et expériences : fonction structure des vitesses
    * simulation numérique directe : dissipation
    * équation d'état bistable : couplage turbulent, lois d'états des Gaz Parfaits, transferts thermiques
    * gravo turbulence

##Partie 2 : Turbulence compressible en aérodynamique
2. mécanique des fluides compressibles transoniques :
    * Kovasznay ondes entropiques, acoustiques, vorticales. Hypothèse de Morkovin turbulence non hypersonique
    * equations de Navier-Stokes compressibles – adimmensionement nombre de Reynolds, de Mach
    * methodes numériques compressibles : schemas conservatifs (Mac Cormak, capture de chocs...), Conditions aux Limites non réfléchissantes,
    * chocs obliques- relations de Rankine Hugoniot
    * turbulence compressible – couche limite dynamique et thermique : Analogie de Reynolds SRA, relations de Crocco Busemann
    * TD numériques : tube à choc, canal plan compressible turbulent, jet plan compressible turbulent

3. turbulence compressible et instrumentation :
    * tuyère, choc oblique type prise d’air, tube à choc,...
    * Mise en évidence expérimentale des modes linéarisés de turbulence compressible (Kovasznay) par l’utilisation du fil chaud et des diagrammes de fluctuation.
