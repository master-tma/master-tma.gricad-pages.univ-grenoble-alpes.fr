Title: M2 TMA Course
menulabel: Courses
Date: 2021-01-17
Category: 4. Course
Slug: formation
lang: en
sortorder: 03

# Objective of the training

The aim is to train scientific experts who have an exhaustive understanding of all
the numerical, experimental and theoretical methods and tools in a wide range of
turbulence applications, from industrial flows to astrophysics, including
geosciences (atmosphere, oceans, rivers), the environment (meteorology, air
quality), aeronautics, energy and transport. Students who choose this course will
want to become experts in fluid mechanics and turbulence before moving on to an
application in a specific field. The M2 TMA offers an innovative pedagogical
approach by refocusing the teaching on the scientific discipline, in this case
turbulence, and all the approaches that allow its analysis, with a unique
interdisciplinary vision.

# Course content

## M2-semester 1: 30 ECTS

### turbulence processes courses : 15 ECTS

- [Theoretical Physics of turbulence]({filename}../modules/physique_theo_turb_en.md)
  3 ECTS ([L. Canet](https://lpmmc.cnrs.fr/spip.php?auteur9))
- [Two phase turbulent flows]({filename}../modules/ecoulement_diph_turb_en.md)
  3 ECTS (
  [P. sechet](https://www.legi.grenoble-inp.fr/web/spip.php?auteur163),
  [G. Balarac],
  [N. Machicoane](http://www.legi.grenoble-inp.fr/people/Nathanael.Machicoane/))
- [Compressible turbulence]({filename}../modules/turb_comp_en.md) 3 ECTS
  ([C. Brun],
  [Pierre Hily-Blant](https://ipag.osug.fr/~hilyblap/index.html#outline-container-orge4b8d74))
- [Dynamo effect and rotation in turbulence]({filename}../modules/dynamo_rotation_turb_en.md)
  3 ECTS
  ([R. Deguen](https://www.isterre.fr/annuaire/pages-web-du-personnel/renaud-deguen/),
  [N. Schaeffer](https://www.isterre.fr/annuaire/pages-web-du-personnel/nathanael-schaeffer/),
  [D. Cebron](https://www.isterre.fr/annuaire/pages-web-du-personnel/david-cebron/))
- [Wave turbulence]({filename}../modules/n_l_ondes_en.md) 3 ECTS
  ([N. Mordant], [P. Augier], [M. Gibert])

### advanced courses: 9 ECTS

- [Advanced experimental methods]({filename}../modules/methode_exp_en.md) 3
  ECTS: Practical work on turbulence (aerodynamic wind tunnel, Coriolis rotating
  tank, wave channel, in situ measurements...) ([M. Gibert],
  [E. Negretti](https://www.legi.grenoble-inp.fr/web/spip.php?auteur227),
  [C. Brun],
  [H. Michalet](https://www.legi.grenoble-inp.fr/web/spip.php?auteur95),
  [N. Mordant],
  [H. Djeridi](https://www.legi.grenoble-inp.fr/web/spip.php?auteur155))
- [Advanced numerical methods]({filename}../modules/methode_num_en.md) 3 ECTS:
  HPC for Navier-Stokes equations (DNS, RANS, LES) ([G. Balarac], [C. Picard],
  [P. Augier](http://www.legi.grenoble-inp.fr/people/Pierre.Augier/))
- [Bilingualism French/English understanding]({filename}../modules/bilinguisme_en.md)
  3 ECTS: bibliographic projects & seminars (Univ Poitiers : P. Moffatt, SDL : S.
  Perraud, CUEF FLE : A. Braibant)

### turbulence and applications courses : 6 ECTS  (1 or 2 courses to be chosen)

- Aerodynamics 3 ECTS :
  [Control and wall turbulence]({filename}../modules/controle_en.md) [M2 TMA]
  (opening 2023)
  ([S. Tardu](https://www.legi.grenoble-inp.fr/web/spip.php?auteur63))
- Applied Mathematics 3 ECTS: GPU for Mathematical Models 
  [M2 MSIAM](https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/master-XB/master-mecanique-IAQK579W/parcours-turbulences-methodes-et-applications-2e-annee-KVQRSPY1/ue-gpu-computing-KZCKTFPZ.html) ([C. Picard],
  [M. Ismail](https://www-liphy.univ-grenoble-alpes.fr/pagesperso/ismail/))
- Astrophysics 3 ECTS: Dynamics of astrophysical plasmas
  [M2 Astrophysique](https://master-physique.univ-grenoble-alpes.fr/le-programme/master-astro/)
  ([J. Ferreira](https://ipag.osug.fr/~ferreirj),
  [B. Cerutti](https://ipag.osug.fr/~ceruttbe/index.html),
  [G. Lesur](https://ipag.osug.fr/~lesurg/))
- Artificial Intelligence 0 ECTS : Introduction to Deep Learning
  [formation CNRS/MIAI](https://gricad-gitlab.univ-grenoble-alpes.fr/talks/fidle/-/wikis/Fidle%20%C3%A0%20distance/Pr%C3%A9sentation)
  ([J.-L. Parouty](https://simap.grenoble-inp.fr/en/research/jean-luc-parouty))
- Geophysics 6 (3+3) ECTS : Waves and instabilities in geophysical fluids
  [M2 STPE](http://formations.univ-grenoble-alpes.fr/fr/catalogue/master-XB/sciences-technologies-sante-STS/master-sciences-de-la-terre-et-des-planetes-environnement-program-master-sciences-de-la-terre-et-des-planetes-environnement/parcours-atmosphere-climat-surfaces-continentales-subprogram-parcours-atmosphere-climat.html)
  ([R. Deguen](https://www.isterre.fr/annuaire/pages-web-du-personnel/renaud-deguen/),
  [D. Cebron](https://www.isterre.fr/annuaire/pages-web-du-personnel/david-cebron/))
- Ocean 3 ECTS:
  [data assimilation in geosciences]({filename}../modules/assimil.md)
  [M2 STPE](https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/master-XB/master-sciences-de-la-terre-et-des-planetes-environnement-IAQK6K1B/parcours-systeme-climatique-atmosphere-hydrosphere-cryosphere-1re-et-2e-annees-KVQRM1RZ.html)
  ([E. Cosme](http://pp.ige-grenoble.fr/pageperso/cosmee/))
- Atmosphere 3 ECTS:
  [Turbulence in the atmospheric boundary layer]({filename}../modules/turb_cla_en.md)
  [M2 TMA] (opening 2023) ([C. Brun],
  [J.M. Cohard](http://www.ige-grenoble.fr/-jean-martial-cohard-))
- Environnement 6 (3+3) ECTS : Numerical Simulation in environmental engineering
  [3A E3 filière HOE](https://ense3.grenoble-inp.fr/fr/formation/ingenieur-de-grenoble-inp-ense3-filiere-hydraulique-ouvrages-et-environnement-hoe#page-presentation)
  ([J. Chauchat](http://www.legi.grenoble-inp.fr/people/Julien.Chauchat/))
- Process Engineering 6 (3+3) ECTS: heat transfer
  [M2 GDP pour l'énergie](https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/master-XB/master-genie-des-procedes-et-des-bio-procedes-IAQKNFHK/parcours-genie-des-procedes-pour-l-energie-IBAWJJZ6/ue-transfert-de-chaleur-IG7XKUEZ.html)
  ([O. Bulliard-Sauret](http://www.legi.grenoble-inp.fr/web/spip.php?auteur239))
- Machine learning in Earth Sciences 3 ECTS : 
[Introduction](https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/master-XB/master-sciences-de-la-terre-et-des-planetes-environnement-IAQK6K1B/parcours-systeme-climatique-atmosphere-hydrosphere-cryosphere-1re-et-2e-annees-KVQRM1RZ/ue-introduction-to-machine-learning-in-earth-sciences-KZ5PF1RS.html) [M2 STPE](https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/master-XB/master-sciences-de-la-terre-et-des-planetes-environnement-IAQK6K1B/parcours-systeme-climatique-atmosphere-hydrosphere-cryosphere-1re-et-2e-annees-KVQRM1RZ.html)  ([L. Moreau](https://www.isterre.fr/annuaire/pages-web-du-personnel/ludovic-moreau), [E. Cosme](http://pp.ige-grenoble.fr/pageperso/cosmee/))

## M2-semester 2 : 30 ECTS

- [M2 Research or R&D internship]({filename}./offres_postes_en.md) 5 months (24 ECTS)
- M1 laboratory internship 2 months (6 ECTS) or 6 ECTS among turbulence and
  applications courses

[c. brun]: http://www.legi.grenoble-inp.fr/web/spip.php?auteur41
[c. picard]: https://christophe.picard.pages.ensimag.fr
[g. balarac]: https://www.legi.grenoble-inp.fr/web/spip.php?auteur57
[m. gibert]: https://neel.cnrs.fr/les-chercheurs-et-techniciens/mathieu-gibert
[m2 tma]: https://master-tma.gricad-pages.univ-grenoble-alpes.fr/
[n. mordant]: http://nicolas.mordant.free.fr/
[p. augier]: http://www.legi.grenoble-inp.fr/people/Pierre.Augier/
