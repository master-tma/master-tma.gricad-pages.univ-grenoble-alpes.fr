Title: Major TMA Structure
menulabel: Structure
Date: 2021-03-09
Category: 3. Structure
Slug: structure-master-tma
lang: en
sortorder: 02

Access to the M2 TMA course is through 3 majors divided into 4 courses, 2 of which are international. The M2 TMA is thus accessible from an M1 in the fields of Physics, Mechanics and Applied Mathematics,
or equivalent, subject to certain prerequisites, in particular an initial training in fluid mechanics and turbulence.
Teaching is in French and English, depending on the modules (see details). The language requirements are a B2 level in oral and written expression for English or French; conversely a B1 level in oral and written comprehension is required for French and English.


![Image]({attach}../figures/structure_TMA_en.png)
