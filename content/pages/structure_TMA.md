Title: Structure Master TMA
menulabel: Structure
Date: 2021-03-09
Category: 3. Structure
Slug: structure-master-tma
lang: fr
sortorder: 02

L'accès au parcours M2 TMA se fait à travers 3 mentions déclinées en 4 parcours dont 2 internationaux. Le M2 TMA est ainsi accessible à partir d'un M1 dans les mentions Physique, Mécanique, Mathématiques et Applications,
ou équivalent,  sous condition de certains prérequis en particulier une formation initiale en mécanique des fluides et turbulence.
L'enseignement est en français et en anglais, selon les modules (voir détail). Les prérequis en langue sont un  Niveau B2  en expression orale & écrite pour  l'anglais ou le français ; réciproquement un niveau B1 en compréhension orale & écrite est requis pour le français et l'anglais .

![Image]({attach}../figures/structure_TMA.png)
