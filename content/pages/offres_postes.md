Title: Offres de stages, thèses, postdoc, CDD/CDI
menulabel: Offres
Date: 2021-01-17
Slug: Offres
lang: fr
sortorder: 05

# Stages M2 (2025)

* [Turbulence océanique : ]({filename}../stages_M2/EC_swot.md) : Post-traitement et exploitation des données de la mission spatiale SWOT pour caractériser la dynamique de surface océanique aux fines échelles (Emmanuel Cosme, Julien Le Sommer, Vadim Bertrand, IGE, Grenoble)

* [Turbulence diphasique : ]({static}../BIBLIO/Prop_M2_et_these__LMFA.pdf) Entraînement d’air dans le sillage d’un cylindre perçant une surface libre (Jean-Philippe Matas, Hélène Scolan, Pierre Trontin, LMFA, Lyon)

* [Turbulence numérique : ]({static}../BIBLIO/stage-C_Friess2024.pdf) Quantification d'incertitude dans le modèle du vent dans un parc éolien (Mitra Fouladirad & [Christophe Friess](https://www.m2p2.fr/annuaire-304/christophe-friess-133.htm), Laboratoire M2P2, Marseille)

* [Turbulence et cavitation : ]({static}../BIBLIO/StageM2_EtudeLH2_ILES_2025.pdf) Modélisation d’écoulements d’hydrogène liquide en condition cryogénique pour l'aviation décarbonée (Eric Goncalves & Philippe Parnaudeau, PPRIME, Poitiers) **POURVU**

* [Turbulence et IA : ]({static}../BIBLIO/stage_fr-multifidelite.pdf) Approches multi-fidélité pour les écoulements turbulents (Thomas Berthelon & [Guillaume Balarac](https://www.legi.grenoble-inp.fr/web/spip.php?auteur57), LEGI, Grenoble)

* [Turbulence et contrôle]({filename}../stages_M2/HJ_aero_vehicule.md) : [Étude en soufflerie de l’aérodynamique du sillage des véhicules routiers  sous condition climatique]({static}../BIBLIO/STAGE-TWIN_LEGI_2025.pdf) ([H. Djeridi](https://www.legi.grenoble-inp.fr/web/spip.php?auteur155), LEGI, Grenoble)

* [Turbulence océanique : ]({static}../BIBLIO/Sujet_Stage_M2_Lapeyre.pdf) Dispersion de traceurs à fine échelle dans l’océan global, (Guillaume Lapeyre, LMD, Paris)

* [Turbulence et pollution atmosphérique : ]({static}../BIBLIO/Stage_M2_CORIA_STG2_25.pdf) CFD modeling and simulation of pollutants dispersion in urban environment. (Talib Dbouk, CORIA, Rouen)

* [Turbulence et contrôle : ]({static}../BIBLIO/Stage_M2_CORIA_TURN_Projet_POPSU_TDv2.pdf) Digital Twin of the Rouen Normandy Metropolis: Physical Models Enrichment. (Talib Dbouk, CORIA, Rouen)

* [Turbulence experimentale : ]({static}../BIBLIO/Internship_FlexDisc_2024_LR.pdf) Deformation of inhomogeneous flexible particle in turbulence. (Gautier Verhille, IRPHE, Marseille)

* [Turbulence experimentale : ]({static}../BIBLIO/Internship_FIF_2024_LR.pdf) Experimental characterization of Fluctuation Induced Force in turbulence. (Gautier Verhille, IRPHE, Marseille)

* [Turbulence numérique : ]({static}../BIBLIO/Stage-turbulence_LES_DNS.pdf) Modélisation et simulation d’écoulements turbulents subsoniques par les approches LES et DNS. (Jérôme Jansen, I2M, Bordeaux) **POURVU**

* [Turbulence et contrôle : ]({static}../BIBLIO/stage_plasma.pdf) Simulation numérique du contrôle des transferts de chaleur sur une rampe par actionneur plasma. (Philipe Traoré, Institut P', Poitiers)

# Pré-Thèses (2024)

* Turbulence optique : [optical turbulence forecasting.](https://sites.google.com/inaf.it/optical-turbulence-group/jobs?authuser=0) ([E. Masciadri](https://sites.google.com/inaf.it/elena-masciadri), [Arcetri Astrophysical Observatory](https://sites.google.com/inaf.it/optical-turbulence-group) in Florence, Italy) **POURVU**

# Thèses (2024)

* [Turbulence en rotation : ]({static}../BIBLIO/PhD_2024_CEBRON.pdf) Experimental turbulence in rotating & stratified geophysical flows. (David Cébron, ISTERRE, Grenoble)

* Turbulence numérique [Predicting the dispersion of accidental atmospheric releases using high-fidelity numerical simulation]({static}../BIBLIO/PhD_offer_CORIA_INERIS.pdf)(Léa Voivenel, CORIA Rouen, Financemenet Région+INERIS) 

* Turbulence numérique [Turbulent particulate flow and invariant solutions]({static}../BIBLIO/these_expe_KIT.pdf) (Markus Uhlmann, Institute for Hydromechanics, Karlsruhe Institute of Technology, Germany, Financement KIT)

* Turbulence expérimentale en soufflerie [Inertial Particle Dynamics in the Turbulent/Non-Turbulent Interface]({static}../BIBLIO/Region_these_lille.pdf) (Martin Obligado, Ecole Centrale Lille, Financement Région)

* Turbulence expérimentale [Etude expérimentale et modélisation de la dynamique d’objets de grande taille dans un écoulement à surface libre : application à la recherche de victimes de noyades en rivières]({static}../BIBLIO/These_LyonLiege_ARCO.pdf) (Nicolas RIVIERE, LMFA, INSA Lyon, Financement ANR)

* [Turbulence expérimentale : ]({filename}../stages_M2/MG_atomization.md) Atomization of a swirled liquid jet by an ultra-high-speed gas flow ([Mathieu Gibert](https://neel.cnrs.fr/les-chercheurs-et-techniciens/mathieu-gibert), Institut Neel Grenoble, **POURVU**)

* Couche limite atmosphérique turbulente : [Caractérisation de la turbulence dans les vents anabatiques sur les pentes des montagnes](https://adum.fr/as/ed/voirproposition.pl?matricule_prop=53685&print=oui), ([Christophe Brun](http://www.legi.grenoble-inp.fr/web/spip.php?auteur41), LEGI Grenoble, Concours ED STEP)

* Turbulence MHD: [Calculs et expériences portant sur des écoulements MHD de métal liquide : application aux pompes électro](https://www.theses-postdocs.cea.fr/job/job-computations-and-experiments-on-liquid-metal-mhd-flows-application-to-electromagnetic-pumps-for-the-so_28967.aspx), (Laurent Davoust, SIMAP Grenoble)

* Turbulence diphasique expérimentale : [RÔLE DE LA TURBULENCE GAZ DANS LA FRAGMENTATION LIQUIDE](https://adum.fr/as/ed/voirproposition.pl?print=oui&matricule_prop=54937), ([Nathanael Machicoane](http://www.legi.grenoble-inp.fr/people/Nathanael.Machicoane/), LEGI Grenoble, Concours ED IMEP2)

* Couche limite atmosphérique turbulente : [Etude des processus d’interactions dynamiques entre l’atmosphère et la fonte nivo-glaciaire, rôle des flux turbulents dans les bilans d’énergie d’un glacier alpin : Hintereisferner, Autriche.](https://adum.fr/as/ed/voirproposition.pl?langue=&matricule_prop=53572&site=edtue), ([Jean-Emmanuel Sicart](https://www.ige-grenoble.fr/-Members-297-), IGE Grenoble, Concours ED STEP)

* Couche limite atmosphérique turbulente : [Couplages dynamiques déterministes vent-vagues pour la couche limite atmosphérique marine]({static}../BIBLIO/Sujet_VentVagues_LHEEA_2024.pdf) (Boris Conan, LHEEA Nantes)

* Convection Turbulente : [Etude expérimentale et numérique de l’ébullition sous-saturée le long d’une plaque verticale en régime de convection naturelle à haut nombre de Rayleigh, caractéristique des piscines de refroidissement de SMR]({static}../BIBLIO/PhD_convection-naturelle-ebullition.pdf) (Michael LE BARS, IRPHE Marseille et Julie-Anne ZAMBAUX, IRSN Cadarache)

* Modélisation numérique de la Turbulence : [Analyse de l’incertitude dans un parc éolien]({static}../BIBLIO/M2P2_Uncertainty_analysis_in_windfarm.pdf) (Mitra FOULADIRAD et [Christophe FRIESS](https://www.m2p2.fr/annuaire-304/christophe-friess-133.htm) et Jérôme Jacob, Laboratoire M2P2 Marseille, Concours ED)

* Modélisation numérique de la Turbulence : [Steady solutions in Lattice-Boltzmann computations of turbulent flows: a method based on selective frequency damping]({static}../BIBLIO/M2P2_Steady_solutions_in_Lattice-Boltzmann.pdf) ([Christophe FRIESS](https://www.m2p2.fr/annuaire-304/christophe-friess-133.htm) et Jérôme JACOB, Laboratoire M2P2 Marseille, Concours ED)

* Turbulence diphasique numérique : [Direct numerical simulation of Boil-off in sloshed cryogenic tanks]({static}../BIBLIO/offre_CNES_boiloff_CORIA.pdf), Benjamin DURET, Laboratoire CORIA Rouen)

# Stages M2 (2024)

* [Turbulence théorique : ]({static}../BIBLIO/stage_previsibilite.pdf) Utilisation des modes de Liapounov pour la prévisibilité atmosphérique (Guillaume Lapeyre, LMD, Paris)

* [Turbulence et énergie : ]({filename}../stages_M2/NO_LES_eolien.md) Simulation aux grandes échelles d’écoulements éoliens ([N. Odier](https://cerfacs.fr/computational-fluid-dynamics/), CERFACS Toulouse)

* [Turbulence diphasique : ]({filename}../stages_M2/NO_LES_liquide_gaz.md) Lois d'état pour la LES d'interfaces liquide-gaz ([N. Odier](https://cerfacs.fr/computational-fluid-dynamics/), CERFACS Toulouse)

* [Turbulence et dispersion de particules : ]({filename}../stages_M2/JIP_dispersion_particules.md) Dispersion de paires de particules
dans un écoulement de turbulence en rotation ([J.I. Polanco](https://jipolanco.gitlab.io), LEGI Grenoble & LMFA Lyon)

* [Turbulence et Intelligence artificielle : ]({filename}../stages_M2/TL_machine_learning_dynamo.md) Machine learning for fluid mechanics: application to dynamo and turbulence ([T. Lehner](https://obspm.academia.edu/ThierryLehner), LUTH, Observatoire de Paris)

* [Turbulence et mathématiques appliquées : ]({filename}../stages_M2/CB_pde_control.md) Model-Free Control of Turbulent Flows ([M. C. Belhadjoudja](https://www.gipsa-lab.grenoble-inp.fr/equipe/infinity), Gipsa-lab Grenoble)

* [Modélisation numérique de la turbulence : ]({filename}../stages_M2/LG_deflagration.md) Comparaison des approches statistiques et de la simulation des grandes échelles pour la déflagration ([L. Gastaldo ](https://www.irsn.fr/recherche/laboratoire-lincendie-explosions-lie#lequipe), IRSN Cadarache)

* [Turbulence diphasique : ]({filename}../stages_M2/AB_bulle_taylor.md) Simulation numérique 3D turbulente d’une bulle de Taylor  ([A. Burlot](https://scholar.google.com/citations?user=a3FNWU0AAAAJ&hl=fr), CEA Saclay)

* [Turbulence expérimentale : ]({filename}../stages_M2/MG_atomization.md) Atomization of a swirled liquid jet by an ultra-high-speed gas flow ([M. Gibert](https://neel.cnrs.fr/les-chercheurs-et-techniciens/mathieu-gibert), Institut Neel Grenoble)

* [Turbulence géophysique : ]({filename}../stages_M2/DC_lunar_core2.md) Turbulent boundary stress in the lunar core ([D. Cebron](https://www.isterre.fr/annuaire/pages-web-du-personnel/david-cebron), ISTerre Grenoble)

* [Turbulence géophysique : ]({filename}../stages_M2/DC_planetary_layers.md) Simulating the role of mountains in planetary fluid layers ([D. Cebron](https://www.isterre.fr/annuaire/pages-web-du-personnel/david-cebron), ISTerre Grenoble)

* [Turbulence et sédiments : ]({filename}../stages_M2/RC_transport_sediment.md) Modélisation RANS du transport sédimentaire en milieu végétalisé ([R. Chassagne](https://www.legi.grenoble-inp.fr/web/spip.php?auteur419), LEGI grenoble)

* [Turbulence et particules : ]({filename}../stages_M2/RM_particle_dynamics.md) Particle dynamics in turbulent flows ([R. Monchaux](https://www.ensta-paris.fr/fr/romain-monchaux), ENSTA-Paris)

* [Turbulence à l'interface océan-atmosphère : ]({filename}../stages_M2/MNB_ocean_atmo.md) Couche de surface atmosphérique marine et houle, analyse à partir d’observations in situ ([B. Conan](https://www.researchgate.net/profile/Boris-Conan), LHEEA Nantes & [M.-N. Bouin](hhttps://www.researchgate.net/profile/Marie-Noelle-Bouin) LOPS Brest)

* [Turbulence cosmique : ]({filename}../stages_M2/YG_rayon_cosmique.md) Transport des rayons cosmiques : turbulence et non-linéarités ([Y. Génolini](https://lapth.cnrs.fr/en/presentation-en), LAPTh Annecy, équipe Astro-Cosmo)

* [Turbulence expérimentale : ]({filename}../stages_M2/MG_vortex_quantique.md) Transition à la turbulence quantique en rotation ([M. Gibert](https://neel.cnrs.fr/les-chercheurs-et-techniciens/mathieu-gibert), Institut Neel Grenoble)

* [Instabilités et transition à la turbulence en astrophysique : ]({filename}../stages_M2/GL_disk_protoplanet.md) Reconnexion magnétique dans les disques protoplanétaires ([G. Lesur](https://ipag.osug.fr/~lesurg), IPAG Grenoble)

* [Turbulence, pollution et météorologie : ]({filename}../stages_M2/TN_polluants_ville.md) Étude numérique de la dispersion de polluant en milieu urbain pour différents quartiers-types ([T. Nagel](https://www.researchgate.net/profile/Tim-Nagel), CNRM Toulouse)

* [Turbulence géophysique : ]({filename}../stages_M2/BM_turb_conv.md) Regional models for turbulent convection in a rapidly rotating shell ([B. Miquel](http://lmfa.ec-lyon.fr/spip.php?article1943), LMFA Lyon)

* [Turbulence océanique : ]({filename}../stages_M2/LM_oceanic.md) Energetics of Jets in Two-Dimensional Oceanic Turbulence ([L. Miller](https://www.ige-grenoble.fr/identite_id1775696.html), IGE Grenoble)

* [Turbulence optique : ]({filename}../stages_M2/JMC_scintillo.md) Measurement of aggregated sensible and latent heat fluxes at km2 scales and their variability over a heterogeneous landscape during the MOSAI experiment. ([J.M. Cohard](http://www.ige-grenoble.fr/-jean-martial-cohard-), IGE Grenoble)

* [Turbulence et IA : ]({filename}../stages_M2/GB_machine_learning.md) Approche multi-fidélité pour les écoulements turbulents ([G. Balarac](https://www.legi.grenoble-inp.fr/web/spip.php?auteur57), LEGI Grenoble)

* [Turbulence atmosphérique : ]({filename}../stages_M2/CB_cocainn.md) Profilage de la couche limite turbulente par sonde pitot 3D Cobra sous le maximum du jet dans le vent catabatique sur pente forte alpine dans la vallée de Innsbruck, Autriche, dans le cadre du projet TeamX ([C. Brun](http://www.legi.grenoble-inp.fr/web/spip.php?auteur41), LEGI Grenoble)

* [Turbulence et courants de gravité : ]({filename}../stages_M2/MR_turbidity.md) To what extent local particle/fluid physical processes drive turbidity currents global dynamics? ([M. Rastello](https://www.legi.grenoble-inp.fr/web/spip.php?auteur296), LEGI Grenoble)

* [Turbulence et applications à la voix humain : ]({filename}../stages_M2/AMVH_jet_human.md) Non-canonical jet dynamics: application to the human upper airways. ([A. M. Van-Hirtum](https://www.legi.grenoble-inp.fr/web/spip.php?auteur343) et X. Pelorson, LEGI Grenoble)

* [Turbulence atmosphérique : ]({filename}../stages_M2/JES_glacier_atmo.md) Etude des processus d’interactions dynamiques entre l’atmosphère et la fonte nivo- glaciaire, rôle des flux turbulents dans les bilans d’énergie d’un glacier alpin: Hintereisferner, Autriche. ([J.-E. Sicart](https://www.ige-grenoble.fr/-Members-297-) et H. Barral, IGE Grenoble)

* [Turbulence expérimentale : ]({filename}../stages_M2/NM_casc_diff_turb.md) Compétition entre cascade et diffusion turbulente ([N. Mordant](http://nicolas.mordant.free.fr/) et Laure Vignal, LEGI Grenoble)

# Thèses (2023)

* Contrôle de la Turbulence : [Influence de la déformation d’un revêtement super-hydrophobe sur les propriétés
d’un sillage turbulent : approche couplée expérimentale et numérique]({static}../BIBLIO/PhDoffer_SuperHydrophobicSurfaces_2023_fr.pdf) ([Nicolas MAZELLIER](https://www.univ-orleans.fr/fr/prisme/nicolas-mazellier), Jean-Christophe ROBINET et Pierre-Yves PASSAGGIA, Laboratoire PRISME Orléans et Laboratoire DYNFLUID ENSAM Paris)

* Modélisation numérique de la Turbulence : [Modélisation algébrique des flux turbulents pour des écoulements avec masse volumique variable en approche RANS et hybride RANS/LES]({static}../BIBLIO/M2P2_RANS_and_hybrid_RANS-LES_algebraic_modeling_of_turbulent_fluxes_in_variable_density_flows.pdf) ([Christophe Friess](https://www.m2p2.fr/annuaire-304/christophe-friess-133.htm) et Fabien Duval, Laboratoire M2P2 Marseille)

* [Turbulence atmosphérique : ]({filename}../theses/LD_atmo_turb.md) Multi-scale interactions in the atmosphere. Application to the prediction of heat waves and urban heat islands ([L. Danaila](https://m2c.cnrs.fr/user/danaila/), M2C, Université de Rouen)

* Turbulence diphasique : [Transport and dispersion of a cluster of particles by a vortex structure]({static}../BIBLIO/PhD_IMFT_AlbagnacViroulet_2023-2026.pdf) (ALBAGNAC Julie & VIROULET Sylvain, Institut de Mécanique des Fluides de Toulouse)
 
# Stages M2 (2023)

* [Turbulence en ingénierie automobile : ]({filename}../stages_M2/JK_LES_aeroacoustics.md) Large Eddy Simulation of the flow around a prototypical car and aeroacoustics interaction ([J. Kreuzinger](https://www.km-turbulenz.de), KM-Turbulenz GmbH, Munich, Germany)

* [Turbulence en ingénierie hydraulique : ]({filename}../stages_M2/SL_cfd_turbine.md) Investigation of the Deep Part Load Behavior of a Francis Turbine via CFD ([S. Leguizamon](https://www.researchgate.net/profile/Sebastian-Leguizamon), GE Renewable Energy Grenoble)

* [Turbulence et méthodes numériques: ]({filename}../stages_M2/GB_turb_stabilite_num.md) Investigation of the stability limits of a linearized implicit scheme for turbulent incompressible flows ([G. Balarac](https://www.legi.grenoble-inp.fr/web/spip.php?auteur57), LEGI Grenoble)

* [Turbulence synthétique et modélisation numérique : ]({filename}../stages_M2/CF_turb_synthetique.md) Vers un forçage pour la turbulence synthétique inhomogène à divergence et hélicité nulles. ([C. Friess](https://www.m2p2.fr/annuaire-304/christophe-friess-133.htm), LM2P2 Marseille)

* [Turbulence atmosphérique et ondes de gravité : ]({filename}../stages_M2/CL_bore_gravite.md)  Étude du coup de vent du 18 juin 2022 sur la Normandie à l’aide de simulations et d’observations à haute résolution ([C. Lac](https://www.umr-cnrm.fr/spip.php?article1220), CNRM Toulouse)

* [Turbulence en ingénierie de l’environnement : ]({filename}../stages_M2/JC_sediment.md) Numerical analysis of flow and morphological patterns around moving objects ([E. Puig Montella](https://www.legi.grenoble-inp.fr/web/spip.php?auteur397), LEGI Grenoble)

* [Turbulence en ingénierie de l’environnement : ]({filename}../stages_M2/JC_hydraulic.md) Numerical simulation of air-entrainment in plunging jets and hydraulic jumps ([J. Chauchat](http://www.legi.grenoble-inp.fr/people/Julien.Chauchat/), LEGI Grenoble)

* [Turbulence superfluide : ]({filename}../stages_M2/PD_particle_helium.md) 3D Particle tracking for the study of superfluid 4He turbulence. (P. Diribarne, [CEA/DSBT](https://www.d-sbt.fr/Pages/Presentation.aspx) Grenoble)

* [Instrumentation en turbulence à haut nombre de Reynolds : ]({filename}../stages_M2/AG_hw_anemo.md) Hot wire anemometry in high Reynolds turbulent flows. (A. Girard, [CEA/DSBT](https://www.d-sbt.fr/Pages/Presentation.aspx) Grenoble)

* [Two-phase flow turbulence : ]({filename}../stages_M2/NM_atomization2.md) The role of gas turbulence in gas-assisted liquid atomization and sprays ([N. Machicoane](http://www.legi.grenoble-inp.fr/people/Nathanael.Machicoane/), LEGI Grenoble)

* [Turbulence & topography : ]({filename}../stages_M2/DC_topo_turb.md) Towards the ocean and planetary cores ([D. Cebron](https://www.isterre.fr/annuaire/pages-web-du-personnel/david-cebron/), ISTerre Grenoble)

* [Turbulence & magnetic field ]({filename}../stages_M2/NS_magnetic_tidal.md) generation by tidal forcing in internal liquid planetary layers ([N. Schaeffer](https://www.isterre.fr/annuaire/pages-web-du-personnel/nathanael-schaeffer/), ISTerre Grenoble)

# Stages M1 (2022)

* [Turbulence stratifiée : ]({filename}../stages_M1/PA_strat_turb.md) Analyse
spatio-temporelle de la turbulence fortement stratifiée forcée dans des modes
rotationels ([P. Augier](http://www.legi.grenoble-inp.fr/people/Pierre.Augier/), LEGI Grenoble)

* [Turbulence et rotation : ]({filename}../stages_M1/NM_onde_turb.md) Strongly
stratified turbulence ([N. Mordant](http://nicolas.mordant.free.fr/), LEGI Grenoble)

* [Transferts thermiques et turbulents : ]({filename}../stages_M1/OB_energetique.md) Etude expérimentale de l’intensification des transferts thermiques par ultrasons ([O. Bulliard-Sauret](https://www.researchgate.net/profile/Odin-Bulliard-Sauret), LEGI Grenoble)

* [In lab turbulent geophysical flows : ]({filename}../stages_M1/EN_ravity_currents.md) Data analysis of experiments on rotating downslope gravity currents ([E. Negretti](https://www.legi.grenoble-inp.fr/web/spip.php?auteur227), LEGI Grenoble)

* [Turbulence in wind tunnel : ]({filename}../stages_M1/MO_active_grid.md) The turbulence properties of active-grid-generated flows ([M. Obligado](http://www.legi.grenoble-inp.fr/web/spip.php?auteur77), LEGI Grenoble)

* [Numerical modeling of turbulence in planetary core : ]({filename}../stages_M1/DC_lunar_core.md) Turbulent boundary stress in the lunar core ([D. Cebron](https://www.isterre.fr/annuaire/pages-web-du-personnel/david-cebron/), ISTerre Grenoble)

* [Two-phase flow turbulence : ]({filename}../stages_M1/NM_spray.md) The role of gas turbulence in gas-assisted liquid atomization and sprays ([N. Machicoane](http://www.legi.grenoble-inp.fr/people/Nathanael.Machicoane/), LEGI Grenoble)

* [Analyse de données in situ de turbulence : ]({filename}../stages_M1/CB_antarctique.md) Turbulence dans les vents catabatiques en Antarctique ([C. Brun](http://www.legi.grenoble-inp.fr/web/spip.php?auteur41), LEGI Grenoble)

* [Turbulence optique : ]({filename}../stages_M1/JMC_scintillometer.md) In situ measurements with a scintillometer ([J.M. Cohard](http://www.ige-grenoble.fr/-jean-martial-cohard-), IGE Grenoble)

