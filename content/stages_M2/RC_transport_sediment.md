Title: Modélisation RANS du transport sédimentaire en milieu végétalisé
Date: 2023-11-21
Category:  Stages M2
lang: fr, en

## Encadrants :

Rémi Chassagne, Julien Chauchat & Cyrille bonamy

## Contact :

remi.chassagne@univ-grenoble-alpes.fr 

julien.chauchat@univ-grenoble-alpes.fr 

cyrille.bonamy@univ-grenpoble-alpes.fr

## Laboratoire :

LEGI

## Date :

février 2024, 6 mois 

## Mots clés : 

Modélisation RANS, transport sédimentaire, Turbulence

## Context :

Les milieux aquatiques végétalisés jouent un rôle important dans la protection de la biodiversité et la préservation morphologique du littoral et des rivières. La compréhension du transport sédimentaire en zone végétalisée est donc un enjeu majeur. Des études récentes en laboratoire (Tinoco and Coco 2018, Yang et al. 2019) ont démontrés que la turbulence générée dans le sillage de la végétation est primordiale dans les mécanismes de re-suspension sédimentaire et que le taux de transport en milieu végétalisé est contrôlé par le taux de turbulence.

## Internship :

L’objectif du stage est d’effectuer des simulations diphasiques (fluide-grains) du transport sédimentaire en milieu végétalisé avec le solver SedFoam, module OpenFoam pour le transport sédimentaire développé au LEGI. La végétation sera classiquement simplifiée en une canopée de cylindres rigides. Une approche RANS sera adoptée en première approche pour prendre en compte la turbulence.
Dans un premier temps, le ou la stagiaire cherchera à reproduire des données issues de la littérature (Yang et al. 2015, Etminan et al. 2018) d’écoulement en canopée sans transport sédimentaire. Des simulations avec sédiments seront ensuite effectuées afin d’étudier la capacité du modèle à prédire le taux de transport et d’analyser les mécanismes moteurs du transport sédimentaire avec végétation. Selon l’avancé du stagiaire et les résultats obtenus, une approche Large Eddy Simulation (LES) pour la turbulence pourra être considérée pour étudier plus finement les interactions turbulence-particules.

## Prérequis :

Un intérêt du candidat pour les écoulement environnementaux et/ou une connaissance des fondamentaux du transport sédimentaire est particulièrement recherché. La personne recrutée doit avoir de solides connaissances/compétences en :
- mécanique des fluides et turbulence.
- modélisation numérique (méthode des volumes finis), une connaissance d’OpenFoam est un plus.
Une motivation pour une poursuite en thèse est également un atout.

## Références :

[1] Etminan, V., R. J. Lowe, and M. Ghisalberti (2017), A new model for predicting the drag exerted by vegetation canopies, Water Resour. Res., 53, 3179–3196, doi:10.1002/ 2016WR020090.

[2] Etminan, V., Ghisalberti, M., & Lowe, R. J. (2018). Predicting bed shear stresses in vegetated channels. Water Resources Research, 54, 9187–9206. https://doi. org/10.1029/2018WR022811

[3] Norris, B. K., Mullarney, J. C., Bryan, K. R., & Henderson, S. M. (2017). The effect of pneumatophore density on turbulence: A field study in a Sonneratia-dominated mangrove forest, Vietnam. Continental Shelf Research, 147, 114–127.

[4] Tinoco, R. O., & Coco, G. (2018). Turbulence as the main driver of resuspension in oscillatory flow through vegetation. Journal of Geophysical Research: Earth Surface, 123, 891–904. https://doi.org/10.1002/2017JF004504

[5] Yang, J. Q., F. Kerger, and H. M. Nepf (2015). Estimation of the bed shear stress in vegetated and bare channels with smooth beds, Water Resour. Res., 51, 3647–3663, doi:10.1002/ 2014WR016042.

[6] Yang, Q. J., & Nepf, H. M. (2019). Impact of vegetation on bed load transport rate and bedform characteristics. Water Resources Research, 55, 6109–6124. https://doi. org/10.1029/2018WR024404


![Image]({static}../figures/Pneumatophores.png)

Figure 1 : Pneumatophores dans une mangrove au Vietnam (Norris et al. 2017)

![Image]({static}../figures/tube_bundle.png)

Figure 2 : Simulations LES d’écoulement à travers un champs de cylindre (adapté de Etminan et al. 2017)
