Title: Non-canonical jet dynamics: application to the human upper airways
Date: 2023-10-05
Category:  Stages M2
lang: fr, en

## Supervisor :

Annemie Vanhirtum & Xavier Pelorson

## Contact :

annemie.vanhirtum@univ-grenoble-alpes.fr 

xavier.pelorson@univ-grenoble-alpes.fr

## Lab :

équipe EDT / LEGI Grenoble

## Date

2024, 5 months.

## Key Words :

fluid mechanics, aero-acoustics, physics, signal processing and instrumentation

## Context :

Jet generation, its dynamics and its consequences, such as the sound produced, are mostly studied for well designed flow and geometrical conditions. Nevertheless, jet formation occurs naturally for a wide range of geometrical and flow scales which can deflect to a large extent from well-designed industrial nozzle flows. In the current proposal we are interested in flow and geometrical conditions characterised by a low Mach number and a moderate Reynolds number (500 < Re < 15000). The resulting flow can be either, laminar, turbulent or transitional so that the flow dynamics is complex and can potentially include the transition regime. These flow conditions are among others characteristic for jet flow formation and dynamics in the upper airways. In this particular case jet formation is closely interlinked with acoustics as vortex and turbulence sound production is an important ingredient of the physics of speech production. It needs no argument that human speech production is important in everyday life and this in every society. This study contributes thus to the fields of both non-canonical jet flow and to the physics of human speech sound production.

## Internship:

Considering the human vocal tract as a time-dependent flow nozzle and waveguide, the nozzle dynamics and jet dynamics are intertwined. The overall objective of this internship is thus a systematic study of the jet dynamics for non-canonical and parameterised conditions representing the human upper airways. Depending on the motivation and competences of the candidate, the internship comprises data acquisition and characterisation (experimental and/or numerical) as well as theoretical modelling.


