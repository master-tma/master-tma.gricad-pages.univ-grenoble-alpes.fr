Title: Regional models for turbulent convection in a rapidly rotating shell.
Date: 2023-10-23
Category:  Stages M2
lang: fr, en

## Supervisor :

Benjamin Miquel

## Contact :

benjamin.miquel@ec-lyon.fr

## Lab :

LMFA, Lyon

## Date

2024, 5-6 months 

## Keywords : 

Numerical, geophysics, Turbulence, convection

## Context :

In both planets, moons, and stars, heat is commonly transported radially outwards by turbulent convection through spherical fluid layers. Nature abounds with examples such as the Earth’s outer core, the oceans within the icy moons Europa and Enceladus, and the convective zone of the Sun, etc. Driven by buoyancy forces resulting from temperature inhomogeneities, these flows are heavily influenced by the rotation Ω of the celestial body. In the limit of rapid rotation (small Rossby number Ro ≪ 1), the geostrophic balance (between the pressure gradient and the Coriolis force) promotes anisotropic structures that preferentially align with the rotation axis [1]. This breaks the spherical symmetry of the planet. For this reason, turbulence and the associated transport are expected to become inhomogeneous with strong variations with the latitude [2].
To alleviate the inherent complexity of the spherical geometry, we will focus on the dynamics of a small patch of fluid in a local, cartesian model that ignores curvature: the tilted rotating Rayleigh–B ́enard convection (tRRBC). In this paradigm, a fluid layer delimited by two flat surfaces is heated from below and cooled from top, and rotates about a tilted axis at an angle q with gravity. This approach has been used extensively for polar regions [3,4] where gravity and rotation are aligned (q = 0), and in equatorial regions [5] (q = π/2).

## Internship :

The focus of this Master internship will be to determine the scaling laws for the heat flux as a function of the temperature difference accross the layer, measured by the Nusselt number Nu and the Rayleigh number Ra, respectively: $$Nu ∼ Ra^{α(q)}$$ 
where the exponent α varies with the colatitude 0 < q < π/2. This parametric study will be carried out using high resolution direct numerical simulations to compute solutions to the Navier-Stokes equations with the parallel spectral solver Coral [6]. Thus we will aim at bridging the gap between the polar and the equatorial scalings by characterizing the transition in the exponent between these two extensively documented limit cases. In addition to the heat flux, the morphology of the large scale structures, that result from the inverse turbulent cascade, will be analysed.

## Prérequis :

The successful applicant will have a strong interest for fundamental and computational Fluid Mechanics, with a background in Physics, Engineering, Applied Mathematics, or Geophysics. The starting date of the internship should be no later than Spring 2024. An ANR funding is secured for a PhD on the same topic starting in Fall 2024. This project is part of collaborations with Wouter Bos (LMFA) and Keith Julien (University of Colorado). Questions about the internship and applications including a CV should be sent to Benjamin Miquel: benjamin.miquel@ec-lyon.fr

## References:

[1] J. Proudman, Proc. R. Soc. Lond. A., 1916

[2] T. Gastine, J. Aurnou, J. Fluid Mech., 2023

[3] S. Stellmach et al., Phys. Rev. Lett., 2014

[4] J. Cheng, J. Aurnou, K. Julien, R. Kunnen, Geophys. Astro. Fluid Dyn., 2018

[5] J. von Hardenberg, D. Goluskin, A. Provenzale, E. Spiegel, Phys. Rev. Lett., 2015

[6] B. Miquel, J. Open Source Soft., 2021


![Image]({static}../figures/rayleigh_benard.png)

Figure 1: Focus on a local fluid patch of turbulent convection in a rotating spherical shell at co-latitude q = 60◦. Visualization: temperature computed numerically for tilted rotating Rayleigh-B ́enard convection, represented in the three planes. Salient features of rotating convection are clearly visible, including the Proudman-Taylor anisotropy along the rotation axis, and the non-isothermal bulk.
