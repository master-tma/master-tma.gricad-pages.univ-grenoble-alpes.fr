Title: Hot Wire Anemometry in high Reynolds turbulent flows
Date: 2022-09-14
Category:  Stages M2
lang: en

## Supervisor :

Alain Girard & Davide Duri

## Contact :

alain.girard@cea.fr

## Lab :

CEA, Département Systèmes Basse Température (DSBT)

## Date

2023, 5-6 months (6 preferred)

## Keywords : 

experimental ; instrumentation ; turbulent flow ; high Reynolds

## Context :

The study of high Reynolds flows has a great interest to study the turbulent cascade, which had been phenomenologically described many years ago (1941) by Kolmogorov in a famous article [1]. High Reynolds flows exhibit a large inertial range, which enables to study the cascade over a broad range of scales. At DSBT, high Reynolds flows are achieved in liquid helium, which exhibits a very low viscosity: thanks to this property, medium size experiments (in the meter range) can reach Re as high as 108. This high Re value however, leads to a very small Kolmogorov (dissipative) scale , in the tens of micron range. This puts a severe constraint on the hot wire design, as the length of the sensor should be comparable to the Kolmogorov scale. So far, only measurements up to the range of the Taylor scale (a few hundreds of microns) could be performed [2]. We plan a factor ten improvement in terms of resolution, to be reached with new microfabricated sensors under development, which will be hopefully available at the end of this year. One of the objectives of this internship consists in characterizing these new hot wires, and comparing them with Wollaston hot wires used so far.


## Internship :

New microfabricated hot wires should be available for the internship, and will be characterized and calibrated during this period. Different sensing materials will be tested: Pt, FeCo, NbN.  The experiments will be performed at DSBT, with liquid helium (technical support is provided, and no preliminary expertise in cryogenics is necessary to start this internship). Depending on the availability of the wind tunnel at LEGI, these hot wires could also be used to characterize high Reynolds room temperature turbulent flows. These hot wires will be compared with the best existing (home made) Wollaston hot wires, whose diameter is as small as 0.5 µm. The characterization will be performed in the HeCal experiment [3] (Fig. 1), specially designed for this purpose: it will enable the calibration (correspondence velocity/voltage), the characterization of the thermal time constant, the influence of the temperature of the flow and of the hot wire itself, etc… These calibrated hot wires will be used to study a high Reynolds cryogenic jet (Hejet, Fig. 2) [4]. Recent measurements in this experiment [2] have shown amazing results when operating the jet at high velocity, and further experiments will be achieved to clarify the behavior of the jet. Experiments in the large cryogenic Von Karman facility [5] (Figure 3), could happen, but this will depend on the availability of the large refrigerator at DSBT.

## References

[1] Kolmogorov Andrei Nikolaevich 1991, The local structure of turbulence in incompressible viscous fluid for very large Reynolds numbers, Proc. R. Soc. Lond. A 434, 9–13, first published in Russian in Dokl. Akad. Nauk SSSR (1941) 30(4)
[2] S. Kharche, Turbulence à Haut Reynolds : Etude de l'intermittence inertielle dans un jet axisymétrique et un écoulement de von Kármán cryogéniques, PhD thesis, Université Grenoble-Alpes, http://www.theses.fr/2021GRALI106
[3] S Kharche et al, HECAL: A cryostat for calibration of hot wires 2020 IOP Conf. Ser.: Mater. Sci. Eng. 755 012078
[4] D. Duri, C. Baudet, P. Charvin, J. Virone, B. Rousset, J.-M. Poncet, and P. Diribarne, Liquid helium inertial jet for comparative study of classical and quantum turbulence, Review of Scientific Instruments 82, 115109 (2011)
[5] B. Rousset et al, Superfluid high REynolds von Kármán experiment, Review of Scientific Instruments 85, 103908 (2014)

![Image]({static}../figures/hecal.png)
Figure 1: the HeCal experiment: left principle and details; right: view of the cryostat

![Image]({static}../figures/hejet.png)
Figure 2: the Hejet experiment [4]

![Image]({static}../figures/shrek.png)
Figure 3 : the SHREK experiment. Left : sketch ; center : inserted in the cryostat ; right : the refrigerator

