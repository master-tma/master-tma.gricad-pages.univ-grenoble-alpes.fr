Title: Atomization of a swirled liquid jet by an ultra-high-speed gas flow
Date: 2023-11-22
Category:  Stages M2
lang: fr, en

## Supervisors :

Mathieu Gibert & Nathanael Machicoane

## Contact :

Mathieu.gibert@neel.cnrs.fr

nathanael.machicoane@univ-grenoble-alpes.fr

## Lab :

Institut Néel, Grenoble & LEGI Grenoble

## Date

2024, 5-6 months 

## Context :

The destabilization of a liquid jet into droplets by a fast gas stream is at the heart of many applications, particularly propulsion (e.g., turbo-reactors and rocket engines). However, existing models and numerical simulations used to dimension these systems have been validated far from the conditions of applications. They cannot describe fragmentation mechanisms in fully turbulent conditions or low surface tension fluids. This project aims to study jet instabilities and droplet formation experimentally in situations where relevant dimensionless numbers are larger than in the fragmentation literature and previous laboratory experiments.

## Internship :

This project will have two phases. The first one with
water/air jets in LEGI, coordinated by N.
Machicoane, and the second one with liquid/gas
nitrogen jets in Institut Néel, coordinated by M.
Gibert. In both cases, we plan on first using a
canonical geometry for the injector and evolving
towards geometries closer to spatial propulsion,
generating a swirled liquid jet. In the first phase, we
propose to study the destabilization of a swirled
water jet by a fast air stream (coaxial injector) and
the ensuing spray formation in conditions of high
Reynolds and Weber numbers at LEGI. Our second
aim is to design a more challenging experiment setup, where the same injector geometries will be used with liquid/gas nitrogen at even larger Re and We. The exploration of the parameter space and the choice of the geometries will be narrowed down to identified candidates benefiting from the previous study in air/water at LEGI. This second set of experiments will rely on the expertise of M. Gibert and J. Vessaire (Institut Néel) in cryogenic experiments. A versatile nitrogen cryostat will be built at Néel, allowing for many experiments at Weber and Reynolds numbers much larger than in the air-water case. Covering a much more comprehensive range of physical parameters will provide an opportunity to unify or invalidate the various existing scaling laws proposed in models for jet instability and drop formation. The cryogenic spray will ultimately be characterized by advanced experimental methods: Phase Doppler Anemometry, optical fiber probes, and holography. In addition, collaborations with JP Matas (LMFA) will leverage his experimental and theoretical expertise in two-phase flows and atomization.

## Required skills:

Fluid dynamics, experimental skills, image analysis, thermodynamics, cryogeny

## Collaboration and networking:

This project is part of a recently funded ANR – CryoSpray - providing support and collaborations. The project consortium comprises theoreticians, numericists, and experimentalists from IN, CEA-SBT, LEGI, and LMFA. The training period will be split between two of Grenoble’s partners: -An experimental part @LEGI to study the destabilization of a swirled water jet by a fast air stream. -A conception part @NÉEL to design and assemble the nitrogen cryostat.

## Possible extension as a PhD:
The ANR – CryoSpray provides the funding for a PhD position. The proposed training period should only be considered by students willing to apply for this PhD position.


![Image]({static}../figures/atomization.png)

Figure 1: Illustration of several atomization regimes in air-water (top, Farago & Chigier 1992) and cryogenic (bottom, Locke et al. 2010) sprays. Similar large-scale features appear for the 3 conditions.
