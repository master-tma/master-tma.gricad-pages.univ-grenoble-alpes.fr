Title: Measurement of aggregated sensible and latent heat fluxes at km2 scales and their variability over a heterogeneous landscape during the MOSAI experiment.
Date: 2023-10-20
Category:  Stages M2
lang: fr, en

## Supervisor :

Jean-Martial Cohard

## Contact :

jean-martial.cohard@univ-grenoble-alpes.fr

## Lab :

IGE, Grenoble

## Date

2024, 5-6 months 

## Keywords : 

experimental ; instrumentation ; turbulent flow ; optical turbulence

## Context :

Evapotranspiration and more generally turbulent fluxes remain particularly difficult to quantify and to model, especially over complex and heterogeneous landscapes. Specifically, climate and atmospheric models rely on surface homogeneity assumptions which most of the time are not valid. This leads to large uncertainties in atmospheric model simulations for boundary layer growth, moisture availability and low level clouds dynamic. Within the MOSAI ANR project, we aim to characterize turbulent fluxes variability at the scale of a climate or meteorological model pixel and evaluate aggregation scheme at model pixel size. Among the project actions, an experiment is schedule at P2OA ACTRISS site near Lannemezan (65) to measure turbulent fluxes with a dozens of turbulent stations on a 5x5km2 area, scintillometers, wind profilers, UAV measurements, ... Specifically, since the 1990s, scintillometry has been recognized as an accurate method to estimate turbulent fluxes at km2 scales compatible with a satellite pixel or a meteorological/hydrological model mesh. 

## Internship :

The internship will aim to operate two scintillometers one in the Microwave domain and one in the Infra-Red domain, together with one flux stations within the context of the MOSAI P2OA experiment. Scintillometers are covering a 0.6km path and measure turbulence produced by dry and wetlands. Scintillometer measurements will be compared with turbulence data from two Eddy-flux towers installed respectively over a wetland and over a grassland located on a ridge to document turbulent fluxes variability associated with the soil moisture. Instruments are installed close to the CRA (Centre de Recherche Atmosphérique - Lannemezan) from April 2023 and delivered data for 3 Intensed observation periods. The instruemntation will be still available for extra measurements along the internship.

The internship will consist in managing the instruments on site, and in processing the data from the three IOPs. The processing work-flow for scintillomettry data have been upgraded and tested in 2022 with success over a crop Mosaic and will be apply for the MOSAI experiment. Eddy-covariance data will be processed using the EddyPro Software. Those data will be further used for comparison with high resolution Large Eddy Simulations atmospheric modeling.

![Image]({static}../figures/scintillo_lannemezan.png)

Figure 1: nfrared and Micro-wave scintillometers installed on the P2OA site in Lannemezan, facing the Pic du Midi.
