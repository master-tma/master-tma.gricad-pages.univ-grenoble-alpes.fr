Title: Étude numérique de la dispersion de polluant en milieu urbain pour différents quartiers-types.
Date: 2023-10-24
Category:  Stages M2
lang: fr, en

## Encadrants :

Tim Nagel (Post-Doctorant MF) & Robert Schoetter (CRDD)

## Contact :

tim.nagel@meteo.fr

## Laboratoire :

Centre National de Recherches Météorologiques (Météo-France/CNRS)

## Date :

2024, 5-6 months 

## Mots clés: 

modélisation numérique, météorologie régionale, pollution atmosphérique

## Contexte :

Les villes sont connues pour avoir une forte influence sur les champs météorologiques locaux, réduisant la vitesse du vent, augmentant la turbulence. Les villes sont également responsables d’importantes émissions de polluants. La concentration, le transport et le temps de séjour des polluants dans les villes sont fortement influencés par leur complexité géométrique. Des valeurs élevées de concentration en polluants ou des longs temps de résidence entraînent des problèmes environnementaux et sanitaires. En restant dans le cadre de la dispersion de polluants considérés comme des scalaires passifs, la plupart des cas d’études considèrent une canopée urbaine extrêmement simplifiée (alignements de cubes) ou très complexe (portions de quartiers réels). Le premier cas permet d’identifier les processus généraux à l’œuvre dans le phénomène de dispersion entre les bâtiments mais l’absence d’hétérogénéités dans la forme ou la hauteur des obstacles ne permet pas de représenter avec réalisme la complexité des écoulements rencontrés en milieu urbain. A l’inverse, un quartier réel est souvent unique et il est difficile de généraliser les résultats observés. Un cas d’étude pouvant faire l’entre-deux semble donc nécessaire.
Météo-France et le Laboratoire d’Aérologie développent depuis 30 ans le modèle de recherche météorologique MesoNH [1]. Celui-ci possède une version où les obstacles comme les bâtiments peuvent être résolus explicitement avec la méthode aux frontières immergées: MesoNH-IBM [2]. Ce modèle a été validé dans différentes configurations comme la dispersion de polluant en milieu urbain idéalisé [3] ou la détermination de quantités aérodynamiques caractéristiques pour différents quartiers-types [4]. Ces derniers permettent de représenter un environnement urbain générique de façon réaliste, incluant une certaine hétérogénéité dans la forme et la hauteur des bâtiments.

## Sujet :

Dans le cadre de ce stage, nous souhaitons caractériser l'exposition aux polluants selon la forme de l'environnement urbain. L’approche par quartiers-types permet de s’affranchir de l’influence d’une configuration urbaine unique sur la dispersion du polluant tout en conservant des cas d’études assez proche de la réalité pour que les résultats puissent être généralisables à toute ville du monde.
Au cours de ce stage, la personne recrutée :
- Lancera des simulations haute résolution de transport de polluant avec MesoNH-IBM pour différents
quartiers-types et différents angles de vent. Dans un premier temps, on considérera une source ponctuelle
de polluant.
- Analysera les résultats, en se focalisant notamment sur les temps de résidence du polluant, la trajectoire des
panache de dispersion, les zones de piégeage du polluant.
- Mettra en place une configuration où le polluant sera émis sur la longueur d’une rue (trafic routier).

## Prérequis :

Pour ce stage, des compétences en mécanique des fluides ou météorologie/océanographie ainsi que une affinité pour la modélisation numérique seront bénéfiques.

## Références:

[1] Lac, C., Chaboureau, P., Masson, V., Pinty, P., Tulet, P., Escobar, J., ... & Aumond, P. (2018). Overview of the Meso-NH model version 5.4 and its applications. Geoscientific Model Development, 11, 1929-1969.

[2] Auguste, F., Réa, G., Paoli, R., Lac, C., Masson, V., & Cariolle, D. (2019). Implementation of an immersed boundary method in the Meso-NH v5. 2 model: applications to an idealized urban environment. Geoscientific Model Development, 12(6), 2607-2633.

[3] Nagel, T., Schoetter, R., Masson, V., Lac, C., & Carissimo, B. (2022). Numerical Analysis of the Atmospheric Boundary-Layer Turbulence Influence on Microscale Transport of Pollutant in an Idealized Urban Environment. Boundary-Layer Meteorology, 184(1), 113-141.

[4] Nagel, T., Schoetter, R., Bourgin, V., Masson, V., & Onofri, E. (2023). Drag Coefficient and Turbulence Mixing Length of Local Climate Zone-Based Urban Morphologies Derived Using Obstacle-Resolving Modelling. Boundary-Layer Meteorology, 186(3), 737-769.
