Title: 3D Particle tracking for the study of superfluid 4He turbulence.
Date: 2022-09-16
Category:  Stages M2
lang: fr, en

## Supervisor :

P. Diribarne, J. Duplat and B. Rousset

## Contact:

pantxo.diribarne@univ-grenoble-alpes.fr@cea.fr

## Lab:

CEA Grenoble, Département Systèmes Basse Température (DSBT)

## Date:

2023, 5-6 months (6 preferred)

## Keywords:

Experiment, superfluid turbulence, particle tracking.

## Context:

The internship will take place in the hydrodynamics team of the Département Systèmes Basse Température at CEA Grenoble which is mostly involved in the investigation of turbulent flows at cryogenic temperatures. The main interest of carrying hydrodynamic experiments at such low temperatures is to take advantage of the very low kinematic viscosity of helium, whether in the normal or superfluid state, to reach high Reynolds numbers in laboratory size apparatus.
One of the subjects of interest is the dissipation of kinetic energy in superfluid turbulent flows. The mechanism by which the kinetic energy is transferred from large scale, where it is injected, to small scales, where it is then converted to heat by viscous friction, is well established and understood in normal fluid state. In the superfluid state though the puzzle still needs to be solved: viscous effects tend to be much less efficient and eventually negligible as the temperature tends towards 0K and the way energy is dissipated at small scale in different temperature regimes is still an important open question that we address in various experiments, mainly through velocity measurements.

## Internship:

The student will work on the so-called OGRES experiment in which a homogeneous and isotropic turbulence is produced by oscillating a pair of parallel grids (see image below and [1] for details) in an optical cryostat containing liquid helium at temperatures ranging from 1.4K to 4.2K. The lagrangian velocity field inside a small volume between the grids is currently measured through 2D particle tracking on hollow glass micro-spheres by means of ultra-high speed cameras. The goal of the internship is twofold: (I) upgrade the optical setup to add a pair of LED-Camera (green line in the sketch below) perpendicular to the current ones and allow simultaneous measurements in two
directions, (ii) use this setup to devise 3D particle trajectories (aka 4D-PTV). We expect to conduct experiments in water first and hopefully at low temperature if time permits.
The student is expected to have a good comprehension of room temperature fluid mechanics, and to be interested learning both state of the art experimental techniques and computer algorithm development for particle tracking.

## References

[1] Sy, F.; Diribarne, P.; Rousset, B.; Gibert, M. & Bourgoin, M. Multiscale energy budget of inertially driven turbulence in normal and superfluid helium, 
Phys. Rev. Fluids, American Physical Society, 2021, 6, 064604

![Image]({static}../figures/PTV_He.png)
