Title: MACHINE LEARNING FOR FLUID MECHANICS: APPLICATION TO DYNAMO AND TURBULENCE.
Date: 2023-12-04
Category:  Stages M2
lang: fr, en

## Encadrants :

Thierry Lehner, Waleed Mouhali & Jae Yun Jun Kim

## Contact :

thierry.lehner@obspm.fr  

waleed.mouhali@ece.fr 

jae-yun.jun-kim@ece.fr   

## Laboratoire :

LUTH, Observatoire de Paris

## Date

2024, 5-6 months 

## Contexte :

La génération et l'inversion du champ magnétique terrestre sont restées l'un des sujets les plus controversés en magnétohydrodynamique. Il est bien connu que le champ magnétique terrestre est généré par l'action dynamo dans le noyau externe de fer liquide. Le champ de vitesse lié au fer liquide dans le noyau crée un champ magnétique grand échelle. Ce mécanisme explique par ailleurs comment un fluide en rotation, en convection et électriquement conducteur entretient un champ magnétique. Il existe de nombreuses data expérimentales et numériques qui tentent de reproduire le phénomène. Dans cette étude, nous nous focalisons sur des data numériques issues de Kreuzahler etal. (2017) [1] qui modélise l’écoulement dit “VKS”.  

## Stage :

Dans ce travail, nous proposons d’avoir recours à une méthode de machine learning pour résoudre le problème dit “de la dynamo inverse” [2] en cartographiant le champ de vitesse à partir du champ magnétique basée sur une méthode de deep learning. Une fois le réseau de neurones et les paramètres de l'algorithme entraînés, l'algorithme optimisé est testé pour estimer le champ de vitesse à partir du champ magnétique mesuré par satellite.  

## Prérequis :

Ce sujet s’adresse aux élèves intéressés par la modélisation physique et les applications du “deep learning” à la physique fondamentale. D’autres développements pourront être réalisés en lien avec le phénomène de la turbulence. Compétences en Deep learning appréciées. 

## References:

[1] Kreuzahler, Sophia, et al. "Dynamo enhancement and mode selection triggered by high magnetic permeability." Physical Review Letters 119.23 (2017): 234501.  

[2] Mouhali, Waleed, Jae-Yun Jun, and Thierry Lehner. "Velocity field reconstruction by Machine Learning during kinematic dynamo process." EGU General Assembly Conference Abstracts. 2022.  


![Image]({static}../figures/dynamo_AI.png)

Figure 1: Dynamo et turbulence 
