Title: Compétition entre cascade et diffusion turbulente.
Date: 2023-09-15
Category:  Stages M2
lang: fr, en

## Responsable de stage :

Nicolas Mordant (enseignant-chercheur UFR UGA)

Laure Vignal (Ingénieur de Recherche LEGI)

## Contact:

nicolas.mordant@univ-grenoble-alpes.fr

## Lab:

LEGI/EDT Grenoble

## Date

5 mois, début en février 2024

## Mots clés : 

Turbulence expérimentale

## Contexte :

Le but du stage est de reproduire expérimentalement la simulation numérique de l’article « How far does turbulence spread ? » de A. Alexakis [1]. Dans une colonne de section carrée, la turbulence est injectée en x=0 (voir figure ci-dessous extraite de l’article) et deux phénomènes ont lieu simultanément. D’une part la turbulence diffuse dans la direction x à cause du transport turbulent. D’autre part, l’énergie cascade dans les échelles pour être dissipée à petite échelle. Au bout d’un certain temps, un régime stationnaire est atteint où les deux phénomènes se compensent.


## Sujet :

Le but du stage est d’étudier cet écoulement dans un nouveau dispositif expérimental en utilisant des mesures par PIV (Particle Image Velocimetry). La cuve est une colonne de 15cmx15cmx1,5m remplie d’eau. La turbulence est excitée par des jets situés à la moitié de la longueur. Le dispositif expérimental étant nouveau, une bonne part du travail sera exploratoire. Dans une première étape, on utilisera de la PIV 2D - 2 composantes pour caractériser l’écoulement et notamment l’établissement de l’écoulement stationnaire. Un travail d’optimisation du dispositif d’injection de la turbulence sera peut-être nécessaire. Ensuite, une étude paramétrique sera mise en œuvre pour étudier les propriétés statistiques de la turbulence en fonction du forçage (et donc en fonction du nombre de Reynolds). On pourra éventuellement mettre en place une PIV stéréo 2D-3C.

## Prérequis : 

instrumentation en mécanique des fluides, turbulence

## References

[1] A. Alexakis https://arxiv.org/abs/2307.08469

![Image]({static}../figures/competition_turbulente.png)
Simulation numérique de la diffusion turbulente dans une colonne rectangulaire.
