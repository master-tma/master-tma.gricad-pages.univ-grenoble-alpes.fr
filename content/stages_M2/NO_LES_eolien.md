Title: Simulation aux grandes échelles d’écoulements éoliens
Date: 2023-12-15
Category:  Stages M2
lang: fr, en

## Responsable de stage :

Nicolas Odier, Laurent gicquel & Florent Duchaine 

## Contact:

nicolas.odier@cerfacs.fr

lgicquel@cerfacs.fr

florent.duchaine@cerfacs.fr

## Lab:

CERFACS Toulouse

## Date

6 mois, à partir du 15 février 2024

## Contexte :

Le CERFACS investigue depuis quelques années la physique des écoulements éoliens, pour le développement de modèles réduits précis. Ces modèles réduits permettent quant à eux la prédiction annuelle de production d'énergie d'un parc éolien.
Différentes configurations d'éoliennes ont été investiguées, avec des approches dites «wall-modelled », qui consistent à inclure la géométrie des pales dans la simulation.
Ces approches permettent une description précise de la turbulence, mais s'avèrent trop coûteuses en termes de coût de calcul.

## Sujet :

L’objectif du stage est l'implémentation d'une méthodologie dite de ligne actuatrice, permettant la représentation des pales par des termes sources dans les équations de conservation. Cette approche permet ainsi de s'abstenir de la prise en compte de la géométrie réelle, en particulier des couches limites, qui induisent des maillages et coûts de calcul importants.
Enfin, si le temps le permet, la prise en compte de la déformation des pales, par la prise en compte des premiers modes propres de déformation des pales, sera investiguée.

![Image]({static}../figures/eolienne.png)

Sillage d'éoliennes en interaction
