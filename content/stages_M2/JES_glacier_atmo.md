Title: Etude des processus d’interactions dynamiques entre l’atmosphère et la fonte nivo- glaciaire, rôle des flux turbulents dans les bilans d’énergie d’un glacier alpin: Hintereisferner, Autriche.
Date: 2023-09-20
Category:  Stages M2
lang: fr, en

## Responsable de stage :

Jean Emmanuel Sicart (IRD, IGE) 

Hélène Barral (IRD, IGE)

## Contact:

jean-emmanuel.sicart@ird.fr

## Lab:

IGE Grenoble

## Date

5 mois, début en février 2024

## Mots clés : 

bilan d’énergie, fusion de neige et glace, flux turbulents, couche limite
atmosphérique, analyse de mesures atmosphériques de la turbulence

## Contexte :

L'objectif est d'étudier les dynamiques spatiale et temporelle de la couche limite atmosphérique et des échange d’énergie associés en surface du glacier Hintereisferner lors d’une campagne de mesure réalisée l’été 2023 dans le cadre du programme international TEAMX [1]et en collaboration avec l’université d’Innsbruck. 

## Sujet :

L’étude consistera à analyser les mesures de station de micro-météorologie et de corrélation turbulente afin de mieux documenter les variations spatiales des flux turbulents, de rayonnements et d’énergie de fonte à la surface du glacier selon les régimes de vent (catabatique / anabatique) et les forçages synoptiques. Elle visera également à tester les schémas de paramétrisations des échanges surface/atmosphère dans les modèles météorologiques et climatiques en milieu de montagne caractérisé par un relief marqué et de forts gradients des variables météorologiques.
La corrélation turbulente nécessite des instruments d’utilisation délicate, à rapide temps de réponse tels qu’un anémomètre sonique et un hygromètre à rayonnement infrarouge. L’étude reposera sur une analyse détaillée des variables de turbulence et d’énergie cinétique turbulente à partir de décompositions de Reynolds des parties moyennes et turbulentes, et des spectres de Fourier des variables turbulentes. Considérée comme une référence dans les études de convection et donnant accès aux variables de l’intensité de la turbulence, la corrélation turbulente reste peu utilisée en milieu de montagne. En complément, l’analyse des profils de vent, de température et d’humidité est nécessaire pour valider les hypothèses de mesure des flux et caractériser la couche de surface.

## Prérequis : 

- Micrométéorologie, analyse des spectres de Fourier, dynamique des fluides géophysiques 

- Programmation

## References

[1] TeamX programme européen d'observation alpine http://www.teamx-programme.org/)

![Image]({static}../figures/glacier_atmo.png)
Observation de la CLA sur le glacier Hintereisferner, Autriche 
