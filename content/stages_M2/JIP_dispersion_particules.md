Title: Dispersion de paires de particules dans un écoulement de turbulence en rotation.
Date: 2023-12-06
Category:  Stages M2
lang: fr, en

## Responsable de stage :

Aurore Naso & Juan Ignacio Polanco

## Contact:

aurore.naso@cnrs.fr 

juan-ignacio.polanco@univ-grenoble-alpes.fr

## Laboratoire :

LMFA Lyon & LEGI Grenoble

## Date :

5 mois

## Contexte :

De nombreux écoulements industriels, géophysiques ou astrophysiques sont turbulents et soumis à un effet de rotation (Fig. 1) : océan, atmosphère terrestre ou jovienne, disques d’accrétion, turbomachines telles que turbines ou turboréacteurs, éoliennes, ... Dans tous ces systèmes, la force de Coriolis modifie la dynamique et la structure du champ turbulent en imposant une direction préférentielle, et par là même une forte anisotropie dans l’écoulement (Fig. 2b).

## Sujet :

On s’intéresse dans ce stage au suivi lagrangien de paires de particules. La dispersion de paires est un sujet d’importance capitale dans les domaines de l’industrie et de l’environnement : elle fournit par exemple une information sur la dispersion de contaminants ou sur le mélange d’espèces dans des réacteurs chimiques.
Nous nous intéressons ici à des particules très petites et de densité égale à celle du fluide, qui suivent donc parfaitement le mouvement de celui-ci. La dispersion relative de ces objets a été largement étudiée dans le cas où la turbulence du fluide porteur est statistiquement isotrope (Sawford, 2001 ; Bourgoin et al., 2006), et plus récemment dans un écoulement de turbulence homogène en rotation (Fig. 2) (Polanco et al., 2023). Nous avons notamment montré par simulation numérique directe que cette dispersion était plus rapide dans la direction de l’axe de rotation que dans les deux autres directions.
L’objectif de ce stage est de poursuivre cette étude, en étudiant numériquement l’asymétrie temporelle de la dispersion de paires, c’est-à-dire le fait que deux particules ne se séparent pas à la même vitesse selon qu’on les suive en avançant ou en remontant le temps (Jucha et al., 2014).
L’étude sera conduite pour différentes valeurs du taux de rotation de l’écoulement et pour différentes séparations initiales entre les particules. Les propriétés des particules (position, vitesse, accélération) le long de leurs trajectoires seront calculées en utilisant un code de simulation numérique directe développé au sein de l’équipe. Il s’agira de traiter ces données en extrayant les statistiques pertinentes. Un sujet de thèse pourra être proposé dans la continuité du stage.

## References

[1] M. Bourgoin et al., The Role of Pair Dispersion in Turbulent Flow, Science 311, 835-838 (2006).

[2] J. Jucha et al., Time-reversal-symmetry Breaking in Turbulence, Phys. Rev. Lett. 113, 054501 (2014).

[3] J. I. Polanco, S. Arun and A. Naso, Multiparticle Lagrangian statistics in homogeneous rotating turbulence, Phys. Rev. Fluids 8, 034602 (2023).

[4] B. Sawford, Turbulent relative dispersion, Annu. Rev. Fluid Mech. 33, 289 (2001).

![Image]({static}../figures/visu_rotation.png)

Figure 1 : Illustrations d’écoulements turbulents affectés par la rotation : de gauche à droite et de haut en bas, vue d’artiste d’un disque d’accrétion entourant une jeune étoile (NASA/JPL-Caltech), cyclone Katrina photographié depuis la station spatiale internationale, éolienne, et turbine à vapeur en cours de montage (Siemens).

![Image]({static}../figures/iso_dispersion.png)

Figure 2 : Visualisation de la dispersion de groupes de quatre particules initialement équidistantes dans des écoulements turbulents (Polanco et al., 2023). La couleur verte représente les valeurs les plus intenses de la vorticité de l’écoulement. (a) Turbulence homogène et isotrope ; (b) turbulence soumise à une forte rotation (l’axe de rotation est aligné avec la direction z).
