Title: Transport des rayons cosmiques : turbulence et non-linéarités
Date: 2023-10-29
Category:  Stages M2
lang: fr, en

## Responsable de stage :

Yoann Génolini & Alexandre Marcowith

## Contact:

yoann.genolini@lapth.cnrs.fr 

Alexandre.Marcowith@umontpellier.fr

## Lab:

Astro-Cosmo (LAPTh) EMA (LUPM) Annecy

## Date

5 mois, début en février 2024

## Mots clés : 

astrophysique, rayons cosmiques, physique non-linéaire, milieu interstellaire

## Contexte :

La Terre est sans cesse bombardée par une pluie de particules énergétiques provenant du Cosmos : les rayons cosmiques (RCs). D’un point de vue astrophysique, l’origine de ces particules relativistes ainsi que les mécanismes de leur accélération et de leur transport restent très peu connus. Pourtant l’enjeu est de taille puisque les rayons cosmiques participent à la dynamique et à la structuration du milieu interstellaire des petites aux grandes échelles.
Pour les rayons cosmiques dits galactiques, on suppose actuellement qu’ils sont accélérés par la propagation d’ondes de choc au cours de la mort de certaines étoiles (les vestiges de supernovæ). Depuis ces sites de production, ces particules chargées voyagent dans la Galaxie en interagissant avec les ondes du plasma turbulent d’abord au voisinage des sources, puis dans le milieu interstellaire, brouillant l’information sur la direction initiale des rayons cosmiques. Le transport effectif des particules est alors de type diffusif et pour le comprendre il est nécessaire de caractériser le tenseur de diffusion.

## Sujet :

Une première partie du stage consistera à étudier numériquement le tenseur de diffusion à l’aide d’une nouvelle technique de simulations particule-test sur GPU. Lorsque la densité de rayons cosmiques est grande, en particulier au voisinage des sources, l’interaction particule-onde étant réciproque, ceux-ci influencent de manière importante la turbulence notamment via l’instabilité de courant. Ce phénomène affecte la densité de rayons cosmiques autour des sources et leur échappement dans le milieu interstellaire. Une deuxième partie du projet de stage, qui pourra être réalisée en thèse, consiste à développer un modèle d’injection réaliste qui tienne compte des effets d’interactions réciproques entre rayons cosmiques et plasma turbulent. Nous étudierons l’influence de différentes phases du milieu interstellaire ainsi que l’influence de théories non- linéaires dans le calcul du tenseur de diffusion.

Vous serez intégré à l’équipe Astrophysique et Cosmologie du LAPTh qui compte plusieurs permanents, des doctorants, post-doctorants et des stagiaires. Les séminaires et journal clubs fréquents font partie de la vie du laboratoire et vous permettront aussi de découvrir le monde de la recherche. Une mission de collaboration avec Alexandre Marcowith au Laboratoire Univers et Particules de Montpellier (LUPM) est prévue lors de ce stage.

## Prérequis : 

Des bases solides en mathématiques, astrophysique, physique des plasmas, physique non-linéaire et programmation sont souhaitables.
Ouverture vers un sujet de thèse : OUI en codirection avec Alexandre Marcowith (LUPM Montpellier)
