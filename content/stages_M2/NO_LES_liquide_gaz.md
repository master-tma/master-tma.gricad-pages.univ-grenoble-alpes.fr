Title: Lois d'état pour la LES d'interfaces liquide-gaz
Date: 2023-12-15
Category:  Stages M2
lang: fr, en

## Responsable de stage :

Nicolas Odier

## Contact:

nicolas.odier@cerfacs.fr

## Lab:

CERFACS Toulouse

## Date

6 mois, à partir du 1er février 2024

## Contexte :

Le CERFACS développe depuis quelques années des méthodes numériques permettant la description des interfaces liquide-gaz, dans un contexte compressible.
Ces méthodes ont été appliquées avec succès pour la simulation aux grandes échelles d'injection de carburant au sein de moteurs aéronautiques, ou spatiaux.
Plusieurs lois d'état peuvent être considérées selon les conditions opératoires pour fermer le système thermodynamique. Des lois d'état décrivant les gaz non-idéaux, dites lois d'état cubiques, peuvent être utilisées dans des conditions de haute pression, et haute température.
Des lois d'état, dites « stiffened-gas » permettent elles la description des liquides à plus basse pression.

## Sujet :

L'objectif du stage sera de quantifier les erreurs induites par ces différentes lois d'état dans une large gamme de points de fonctionnement, depuis des conditions froides, basse pression, jusqu'à des conditions hautes pression, haute température. Des configurations d'interfaces planes, de gouttes isolées, et éventuellement de jets diphasiques seront investiguées.

## Rérequis :

Le candidat devra avoir une bonne connaissance de la thermodynamique, un goût prononcé pour la physique.

![Image]({static}../figures/cm_eau_air.png)

LES d'interface de couche de mélange eau-air
