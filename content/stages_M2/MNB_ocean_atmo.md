Title: Couche de surface atmosphérique marine et houle, analyse à partir d’observations in situ
Date: 2023-10-30
Category:  Stages M2
lang: fr, en

## Responsable de stage :

Boris Conan (MCF, LHEEA), Marie-Noëlle Bouin (Chercheur, CNRM/LOPS), William Bruch (postdoctoant LHEEA) & Louis Marié (Chercheur, LOPS)

## Contact : 

boris.conan@ec-nantes.fr, nbouin@ifremer.fr 

## Laboratoire : 

LHEEA (Laboratoire de Recherche en Hydrodynamique, Energétique et Environnement Atmosphérique), EC Nantes, en collaboration avec le LOPS (Laboratoire d’Océanographie Physique et Spatiale), Brest.

## Date : 

5 mois, début février 2024

## Mots clés :

interface océan atmosphère, observation, modélisation, turbulence

## Contexte :

Contrairement à la mer du vent qui est directement couplée au vent local, la houle se propage plus vite que le vent de surface et peut avoir des directions très différentes. Les déformations de la surface qu’elle provoque ont un effet rétroactif à la fois sur les petites vagues et sur l’écoulement atmosphérique à la surface : ondulations du champ de pression, modifications du profil de vent à la surface, voire création d’un jet de basses couches. Cette rétroaction a un effet sur le transfert de quantité de mouvement entre l’atmosphère et les vagues, qui est du second ordre par rapport à celui du à la croissance des vagues, mais qui peut devenir importants dans certains cas : cyclones tropicaux avec houle forte, à évolution rapide et désalignée, situations calmes dans les Tropiques, mers croisées dans tempêtes extratropicales, échelles climatiques. Les flux de chaleur et de quantité de mouvement étant interdépendants, la présence de houle est suspectée d’avoir une influence sur la représentation des processus d’intensification des cyclones dans les modèles de prévision, et de l’évolution des échanges de chaleur océan-atmosphère à long terme dans les modèles de climat. 
Ces effets de la houle ont été observés in situ avec des bouées, reproduits en bassin, formalisés et simulés avec des simulations aux grands tourbillons (LES), mais sans arriver à une représentation exhaustive et basée sur des observations des profils de vent à la surface. Le LHEEA dispose d’un jeu de données très riche de mesures in situ de ce type de profils, acquis sur plusieurs mois et des situations très variées de houle, vent, et stratification atmosphérique à partir d’un Lidar scannant installé à Belle Île. Ce jeu de données permet d’avoir accès non seulement au vent près de la surface, mais aux spectres d’états de mer, aux directions respectives et aux conditions atmosphériques détaillées (stratification de la couche limite, présence de couches internes) et à leur évolution.

## Sujet :

Les objectifs de ce stage sont de : 

* proposer une classification des profils de vent dans la couche d’influence de la houle, en fonction des conditions environnantes (vent, états de mer, directions relatives, stabilité)

* interpréter les résultats par rapports à la littérature, aussi bien par rapport aux observations in situ existantes, qu’aux expériences en bassin, et aux sorties de simulations idéalisées

* dériver des paramètres d’échelle appropriés pour la représentation de ces profils (âge des vagues, pente, échelles de stabilité ou de turbulence)

Compte tenu du caractère extrêmement novateur de ces données et selon les résultats du stage, ce travail est susceptible d’aboutir à une publication. Il pourra également déboucher sur une thèse au LOPS (Brest) en co direction  avec le LHEEA (M.N. Bouin, B. Conan), sur le développement d’une paramétrisation de l’effet de la houle sur les flux turbulents air mer, pour les modèles atmosphériques et sa validation sur des études de cas. Les situations d’intérêt sont celles de la campagne SUMOS (2021, Golfe de Gascogne) pour lesquelles on dispose de nombreuses observations satellite ou in situ, et que l’on cherchera à reproduire à partir de simulations couplées LES atmosphère vagues.

## Prérequis : 

Maîtrise d’outils d’analyse type Python, dans un environnement Linux. Connaissances de la physique des écoulements de couche limite. Des connaissances de base de la physique des états de mer, des écoulements atmosphériques sont un plus.
