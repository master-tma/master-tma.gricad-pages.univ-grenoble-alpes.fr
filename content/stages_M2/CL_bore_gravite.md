Title: Étude du coup de vent du 18 juin 2022 sur la Normandie à l’aide de simulations et d’observations à haute résolution.
Date: 2022-11-14
Category:  Stages M2
lang: fr, en

## Responsable de stage :

 Lac Christine (CNRM/GMME) et Mandement Marc (CNRM/GMME/PRECIP)

## Contact :

christine.lac@meteo.fr 05 61 07 96 02

marc.mandement@meteo.fr 05 61 07 90 44

## Laboratoire :

Centre National de Recherches Météorologiques (Météo-France/CNRS)

## Date

2023, 5-6 months (6 preferred)

## Mots Clés: 

turbulence atmosphérique, ondes de gravité, Méso-NH

## Contexte :

Durant la soirée du 18 juin 2022, un coup de vent s’est produit après une journée estivale sur la côte Fleurie en Normandie, provoquant le décès d'un kitesurfeur à Villers-sur-Mer et plusieurs blessés atteints par des projectiles. Ce phénomène s’est manifesté par une brusque augmentation de la vitesse et une rotation du vent près du sol, associées à des nuages en rouleau entrecoupés de ciel clair (https://mobile.twitter.com/isabellev_po/status/1538483006146494466). Les premières analyses des observations indiquent qu’il pourrait s’agir d’une bore, associée à des ondes de gravité générées bien à l’avant d’une activité convective sur la Manche, et se propageant dans la couche limite stable marine de la baie de Seine. Ce phénomène de bore, déjà observé sur la région des Grandes Plaines aux États-Unis, a fait l’objet de quelques études permettant de mieux décrire les processus dynamiques le générant (Hartung et al., 2010 ; Haghi et al., 2019 ; Haghi et Durran, 2021). Dans notre cas, le contexte semble assez différent, car côtier : il s’agit donc de bien caractériser le phénomène, comprendre l’origine des ondes de gravité (convection ou autre) et identifier le phénomène d’amplification (piégeage, déferlement, encaissement en fond de baie de Seine, ...) menant à des intensités extrêmes. De plus, le phénomène de bore est habituellement associé à un faible changement de température près de la surface, voire un léger réchauffement (Haghi et al., 2019) : dans ce cas du 18 juin, on observe près de la surface un refroidissement rapide de la température à l’arrivée du train d’ondes, ce qui pourrait indiquer que le phénomène de bore n’est pas le seul impliqué dans le coup de vent.
L’évènement a été extrêmement difficile à prévoir : même si les modèles de prévision numérique du temps AROME (Brousseau et al., 2016) et AROME-PI (Auger et al., 2015) avaient bien prévu la rotation des vents, l’intensité des vents était sous-estimée et ne permettait pas de déceler les caractéristiques de fine échelle et les intensités exceptionnelles observées. On cherchera donc également à estimer la résolution spatio-temporelle nécessaire à la simulation de ce type d’évènement.
Pour progresser sur ces questions, le stage s’appuiera d’une part sur l’analyse des observations conventionnelles et issues d’objets connectés disponibles le 18 juin, et d’autre part sur des simulations avec le modèle numérique de recherche Méso-NH (Lac et al., 2018) en descente d’échelle depuis AROME, à 500 m et 100 m voire moins. L’objectif sera d’identifier les processus pilotant l’évènement, et de caractériser l’activité ondulatoire observée et simulée. On approfondira aussi les origines possibles de la génération des ondes de gravité et de l’amplification du phénomène. On pourra s’appuyer pour les simulations sur différents diagnostics de décomposition spectrale et de caractérisation des ondes (spectres d’énergie selon Ricard et al., 2013 ; diagrammes d’Hayashi selon Lac et al., 2002 ; paramètre de Scorer).


## Internship :

Au cours du stage, le(la) candidat(e) réalisera les travaux suivants :
- étude bibliographique sur les origines possibles du phénomène, dont la bore ;
- traitement et analyse des observations conventionnelles et d’objets connectés de l’évènement du 18 juin ;
- simulations Méso-NH avec grilles imbriquées jusqu’à 100 m au moins ;
- analyse spectrale des champs dynamiques simulés.

## References

[1] Auger, L., Dupont, O., Hagelin, S., Brousseau, P. et Brovelli, P. (2015), AROME–NWC: a new nowcasting tool based on an operational mesoscale forecasting system. Quart. J. Roy. Meteorol. Soc., 141, 1603-1611. https://doi.org/10.1002/qj.2463
[2] Brousseau, P., Seity, Y., Ricard, D. et Léger, J. (2016), Improvement of the forecast of convective activity from the AROME-France system. Quart. J. Roy. Meteorol. Soc., 142, 2231-2243. https://doi.org/10.1002/qj.2822
[3] Haghi, K. R., Geerts, B., Chipilski, H. G., Johnson, A., Degelia, S., et al. (2019). Bore-ing into nocturnal convection. Bulletin of the American Meteorological Society, 100(6), 1103-1121. https://doi.org/10.1175/BAMS-D-17-0250.1
[4] Haghi, K. R., et Durran, D. R. (2021). On the dynamics of atmospheric bores. Journal of the Atmospheric Sciences, 78(1), 313-327. https://doi.org/10.1175/JAS-D-20-0181.1
[5] Hartung, D. C., Otkin, J. A., Martin, J. E., et Turner, D. D. (2010). The life cycle of an undular bore and its interaction with a shallow, intense cold front. Monthly weather review, 138(3), 886-908. https://doi.org/10.1175/2009MWR3028.1
[6] Ricard, D., Lac C., Riette S., Legrand R., et Mary A. (2013). Kinetic energy spectra characteristics of two convection-permitting limited-area models AROME and Meso-NH, Quart. J. Roy. Meteorol. Soc., 139, 1327-1341. https://doi.org/10.1002/qj.2025
[7] Lac, C., Lafore J.-P., et Redelsperger J.-L. (2002). Role of gravity waves in triggering deep convection during TOGA COARE, J. Atmos. Sci., 59, 1293-1316. https://doi.org/10.1175/1520- 0469(2002)059<1293:ROGWIT>2.0.CO;2
[8] Lac, C., Chaboureau, J.-P., Masson, V., Pinty, J.-P., Tulet, P., Escobar, J., Leriche, M., Barthe, C., et al. (2018). Overview of the Meso-NH model version 5.4 and its applications, Geosci. Model Dev., 11, 1929–1969. https://doi.org/10.5194/gmd-11-1929-2018

![Image]({static}../figures/nuages.png)
Figure 1: Le 18 juin 2022, coup de vent sur la côte Fleurie en Normandie, provoquant le décès d'un kitesurfeur à Villers- sur-Mer et plusieurs blessés.

![Image]({static}../figures/netamo.png)
Figure 2: A l’avant de cellules convectives sur la Manche, nuages en rouleaux avec brusque augmentation du vent et rotation, associées à des ondes de gravité
