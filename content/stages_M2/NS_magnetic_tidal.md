Title: Turbulence and magnetic field generation by tidal forcing in internal liquid planetary layers
Date: 2022-09-11
Category:  Stages M2
lang: fr, en

## Supervisor :

Nathanael Schaeffer & David Cébron 

## Contact :

nathanael.schaeffer@univ-grenoble-alpes.fr

david.cebron@univ-grenoble-alpes.fr

## Lab :

équipe Geodynamo / ISTerre Grenoble

## Date

2023, 5 to 6 months.

## Key Words :

direct numerical simulations; tides; turbulence; rotation; dissipation; geophysical flows

## Context :

Tides excite large scale flows in the internal fluid envelopes of planets, like the Earth's core or global subsurface oceans of Saturn's moon Enceladus, for example.
If the tides are strong enough, turbulence can be triggered through a violent instability. This turbulence can dissipate a lot of energy and possibly influence the astronomic evolution of planet-moon systems (distance, period).
Moreover, if the fluid is eclectrically conducting, such turbulence may lead to the spontaneous generation of a planetary magnetic field (dynamo effect).
For the Earth's core, it has been advocated that tides could have start the geodynamo a few billion years in the past (Landeau+ 2022).
However, little is known about the turbulence intensity or the magnetic field strength and geometry (dipolar or not) of such a flow.
Furthermore, although two regimes of turbulence were reported (small-scale wave turbulence and geostrophic flows -- Le Reun+ 2019), it is unclear how these translate into a global and realistic spherical geometry.

## Project :

The goal is to perform 3D numerical simulations in a spherical shell using the XSHELLS code and/or Nek5000, in order to investigate first the strength of the turbulent flow produced by tidal instabilities, as well as the corresponding dissipation.
These will be novel results that will allow to constain the astronomical evolution of planet-moon systems when there is a liquid layer. In addition, the characterization of the possible two regimes of turblence (see above) would also help to settle some debates.
If time permits, or for a possible PhD project afterwards, two other questions can be addressed: (i) under which conditions can these flow generate a magnetic field? (ii) what is the intensity and geometry of such magnetic fields, and how much energy do they dissipate?

This study involves running simulations (numerical experiments) using an existing code, analysing the results (mostly using python), drawing conclusions and implications for planets and moons.
The tidal forcing can trigger turbulent flows only for non-perfectly spherical layers (deformed spheres). The simulations with XSHELLS are efficient but rely on some approximation to mimick an ellipsoid in a sphere. The quality of this approximation should also be quantified, either by comparing with Nek5000 (a much less efficient code, but which can actually simulate the correct ellipsoidal geometry) or to theoretical results provided by an in-house code developed in the group by J. Vidal.

![Image]({static}../figures/view_W3D_fluidaxis3.jpg)
Ecoulement turbulent et champ magnétique simulé dans une sphère avec le code xshells.
