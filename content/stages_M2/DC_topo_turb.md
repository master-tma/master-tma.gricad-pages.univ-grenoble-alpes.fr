Title: Turbulence and topography : towards the ocean and planetary cores
Date: 2022-09-11
Category:  Stages M2
lang: fr, en

## Supervisor :

David Cébron

## Contact :

david.cebron@univ-grenoble-alpes.fr

## Lab :

ISTerre Grenoble

## Date

6 months from february 2023, possibly followed by a PhD thesis (2023 - 2026)

## Context: 

relying on geophysical observations, it is possible to constrain the current dissipa- tions in the liquid cores of the Earth and the Moon. But these measured dissipations, expected to be dominant near the fluid core boundaries, are in disagreement with the latest dynamical models. These two puzzling measurements can be attributed to the limitations of our models: we know little about the boundary stresses generated by a turbulent rotating flow in presence of buoyancy and topographic effects. Beyond planetary cores, such models are relevant for oceanic and atmospheric flows, where these effects modifies the stress on the ground floor.

## M2 project: 

the ERC THEIA group have designed an experimental setup, where a tank is rapidly rotating and filled with salty water of variable density (creating a density profile). This setup should be built before December 2022, and will provide a solid basis for a planned larger scale experiment (which will be built later, during the associated PhD thesis 2023- 2026). The student will perform dedicated laboratory experiment, complemented with nu- merical and theoretical models, to investigate topographic and buoyancy effects.

## Duration & funding: 

this M2 internship will be funded for at least 6 months, and can be then pursued in PhD (3 years funding by the ERC THEIA, for a start during the fall 2023). This research topic is directly related with the general topic “topographic coupling” currently investigated within the group Geodynamo. The student, with skills in fluid mechanics, will thus naturally collaborate with other members of the team, e.g. on theoretical aspects with the 3rd year PhD student R. Monville, Dr D. Jault or Dr J. Vidal. 
Funded by the European Research Council (ERC) under the European Union’s Horizon 2020 research and innovation programme (grant agreement no. 847433)

![Image]({static}../figures/topo_turb_lam.png)
Laminar flow (before the onset of turbulent flows). Copyright: R. Monville

![Image]({static}../figures/topo_turb_exp.png)
Experimental setup. Copyright: Max Solazzo.
