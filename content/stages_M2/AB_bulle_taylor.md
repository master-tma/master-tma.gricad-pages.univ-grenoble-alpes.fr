Title: Simulation numérique 3D turbulente d’une bulle de Taylor
Date: 2023-11-22
Category:  Stages M2
lang: fr, en

## Responsable de stage :

Alan Burlot & Nathanael Machicoane

## Contact:

alan.burlot@cea.fr

nathanael.machicoane@univ-grenoble-alpes.fr

## Lab:

CEA Saclay

## Date

6 mois, début en mars 2024

## Contexte :

La mécanique des fluides numérique (CFD) est aujourd’hui un outil indispensable dans l’élaboration, la conception, la validation et l’étude de dispositifs industriels. L’étude des écoulements diphasiques, notamment pour les piles à combustible, la production d’hydrogène et les réacteurs à eau pressurisée, est un enjeu majeur pour évaluer les performances et les risques liés au fonctionnement en régime bouillant.

Dans le cadre d’une collaboration avec le Jožef Stefan Institute à Ljubljana en Slovénie, des mesures expérimentales sur l’écoulement d’une bulle de Taylor ont été effectuées. Dans une installation nucléaire, de telles bulles de vapeur peuvent apparaître dans des conduites et fortement réduire le débit. Il est important de pouvoir modéliser correctement ce type de phénomènes dans un code de calcul simulant l’ensemble d’une centrale nucléaire. Notre objectif est donc de produire des simulations précises d’une bulle de Taylor afin de tester les corrélations prenant en compte l’effet d’un bouchon dans un code de calcul à plus grandes échelles.

## Sujet :

L'objectif est de comprendre la dynamique d'une bulle de Taylor avec des simulations numériques précises et d'extraire des informations physiques sur sa dynamique afin de confronter des modèles de perte de pression ou de vitesse.
Ce stage s’inscrit dans un programme de recherche initié en 2021. Il se positionne au cœur des travaux visant à utiliser les simulations les plus précises (DNS) pour améliorer les modèles des simulations les plus macroscopiques afin d’améliorer les codes de calcul industriel. Plusieurs étapes ont permis de valider les procédures de calcul en 2D axisymétriques laminaire, en 3D laminaire puis en 3D turbulent. C’est sur la base de cette dernière étape que le présent travail s’inscrit. L’objectif de ce stage est de poursuivre les simulations turbulentes en explorant plus finement l’effet de la turbulence sur la dynamique de l’écoulement. En particulier, une méthode de génération de turbulence via une boîte de recirculation sera mise en place et comparé aux résultats actuels utilisant une méthode de génération de turbulence synthétique. Une série de simulation sera lancée pour faire varier les propriétés physiques de l’écoulement et ainsi évaluer des corrélations sur les résultats (chute de pression, variation de débit). En fonction de l’avancement du stage et de l’intérêt du stagiaire, différents axes pourront être explorés. Il serait par exemple intéressant d’améliorer la méthode de génération de turbulence pour mieux tenir compte d’effet d’anisotropie. Explorer l’effet de la bulle sur le mélange en ajoutant un champ de température permettrait d’étendre la modélisation à des conditions plus proches du cas réel. Une thèse est proposée en poursuite directe du stage.

## Prérequis : 

Simulation, mécanique des fluides, thermohydraulique, notions de turbulence ou sur les écoulements diphasiques. Une connaissance de l’environnement Linux serait un plus.

## Moyens / Méthodes / Logiciels
TrioCFD, python, VisIt, Linux, supercalculateur

