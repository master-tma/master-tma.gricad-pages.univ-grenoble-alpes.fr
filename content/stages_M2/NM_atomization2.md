Title: Gas-assisted liquid atomization and sprays, the role of gas turbulence
Date: 2022-09-11
Category:  Stages M2
lang: fr, en

## Supervisor :

Nathanaël Machicoane 

## Contact :

nathanael.machicoane@univ-grenoble-alpes.fr

## Lab :

équipe EDT / LEGI Grenoble

## Date

2023, 5 months.

## Key Words :

experimental; turbulent two-phase flows; data and image processing

## Context :

Liquid fragmentation is a highly complex multiscale and multiphysics problem where a liquid phase is broken up by a gas phase into a cloud of fine droplets (spray). Relevant non-dimensional parameters have yet to be obtained to fully characterize spray formation physics. Previous studies have not accounted for varying gas turbulence levels, leading to contradictions between reported spray parameters. There is a need to better understand the role of gas turbulence in liquid fragmentation to inform prediction and model development.
This project, recently funded by ANR, aims at understanding fragmentation mechanisms over a wide range of gas turbulence intensities. A novel fragmentation experiment will explore fragmentation from gas flows with low turbulence to flows where the turbulent fluctuations are solely responsible for break-up. Additionally, atomization will be studied in both planar and coaxial configurations. These three set-ups will allow a newfound understanding of turbulent fragmentation mechanisms, encompassing both primary and secondary break-up processes.

## Internship:

The goal is to investigate different levels of turbulent fluctuations in the gas flow, and how turbulence affects the fragmentation mechanisms. These include the initial interfacial and large-scale instabilities of the liquid phase and the subsequent break-up events into droplets. The study will require the combination of a set of available measurements techniques and the development of analysis tools. Hotwire anemometry and/or particle image velocity will be used to characterize the gas turbulence, while high spatial and temporal resolution imaging will help study fragmentation. The experimental techniques used will rely on high-speed imaging cameras, optical probes, and the associated processing techniques.
For M2 students, the internship can readily transition into a PhD.

The following reading materials give a good positioning of the project:
- Matas, J. P., Marty, S., Dem, M. S., & Cartellier, A. (2015). Influence of gas turbulence on the instability of an air-water mixing layer. PRL, 115(7), 074501.
- Kaczmarek, M., Osuna-Orozco, R., Huck, P. D., Aliseda, A., & Machicoane, N. (2022). Spatial characterization of the flapping instability of a laminar liquid jet fragmented by a swirled gas co-flow. IJMF, 152, 104056. Visualizations of the fragmentation of a liquid jet by a gas jet are available [here](https://www.youtube.com/watch?v=tAVFWx9VQeQ%2F)

![Image]({static}../figures/NM2_spray.png)
