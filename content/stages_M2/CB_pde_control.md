Title: Model-Free Control of Turbulent Flows.
Date: 2023-11-22
Category:  Stages M2
lang: fr, en

## Supervisors:

Mohamed Camil Belhadjoudja, Mohamed Maghenem & Emmanuel Witrant

## Contact:

mohamed.belhadjoudja@grenoble-inp.fr

mohamed.maghenem@grenoble-inp.fr

emmanuel.witrant@grenoble-inp.fr

## Lab:

Gipsa-lab, Grenoble

## Date:

2024, 5-6 months 

## Keywords: 

Control theory; Lyapunov methods; Partial differential equations; Navier-Stokes equations; Model-Free control; Numerical analysis.

## Context:

Partial differential equations (PDEs) are mathematical equations that describe phenomena evolving with respect to multiple variables, such as space and time [4, 8]. Navier-Stokes PDEs describe the evolution in space and time of fluids. In particular, these equations are an accurate model for turbulence. Turbulence control, or turbulence attenuation, could be performed by controlling Navier-Stokes equations. Navier-Stokes equations are multidimensional convective parabolic PDEs. 

## Internship:

We developed a method, based on Lyapunov theory, to control a class of scalar convective parabolic PDEs [1, 2, 3]. We believe that our approach can be extended to multidimensional convective parabolic PDEs, such as Navier-Stokes equations. The extension of our approach may have a significant impact in practice, as turbulence attenuation is of great importance in many domains such as aircraft and spacecraft control. On the other hand, Navier-Stokes equations comprise well-known (stabilizing) terms such as convection and diffusion. However, fluids are also affected by (eventually destabilizing) forcing terms that may be difficult to model accurately. These unknown functions that represent external forces appear in Navier- Stokes equations. We would like to explore the possibility of rewriting our controller in a ”model-free form”, i.e in a form that does not require the knowledge of the destabilizing forcing terms that act on the PDE [6, 5, 7]. This may bring challenges to the stability analysis. Finally, to validate our approach, it would be interesting (depending on the time allowance) to study the well-posedness of the closed-loop system, as well as produce some significant numerical simulations by exploring different techniques.

## Required skills:

solid background in mathematics, control theory, and numerical analysis, and excellent writing and communication in English. A detailed CV (2 pages max) including grades and ranking must be provided.

## References:

[1] M. C. Belhadjoudja et al. “Adaptive Stabilization of the Kuramoto-Sivashinsky Equation Subject to Intermittent Sensing”. In: 2023 American Control Conference (ACC). 2023, pp. 1608–1613. doi: 10.23919/ACC55779.2023. 10155853.

[2] M. C. Belhadjoudja et al. “From Sontag’s to Cardano-Lyapunov Formula for Systems Not Affine in the Control: Convection-Enabled PDE Stabilization”. In: submitted to the American Control Conference (invited paper) (ACC 2024).

[3] M. C. Belhadjoudja et al. “Inverse Optimal Cardano-Lyapunov Feedback for PDEs with Convection”. In: sub- mitted to the European Control Conference (ECC 2024).

[4] Lawrence C Evans. Partial differential equations. Vol. 19. American Mathematical Society, 2022.

[5] Michel Fliess and C ́edric Join. “Model-free control”. In: International Journal of Control 86.12 (2013), pp. 2228–2252.

[6] Michel Fliess and C ́edric Join. “Model-free control and intelligent PID controllers: towards a possible trivialization of nonlinear control?” In: IFAC Proceedings Volumes 42.10 (2009), pp. 1531–1550.

[7] Jingqing Han. “From PID to active disturbance rejection control”. In: IEEE transactions on Industrial Electronics 56.3 (2009), pp. 900–906.

[8] Miroslav Krstic and Andrey Smyshlyaev. Boundary control of PDEs: A course on backstepping designs. SIAM, 2008.


![Image]({static}../figures/streamlines_voilier.png)

Figure 1: Streamlines around a sailboat. An application of the results obtained during this Masters' internship is the attenuation of turbulence around moving bodies.
