Title: Post-traitement et exploitation des données de la mission spatiale SWOT pour caractériser la dynamique de surface océanique aux fines échelles
Date: 2024-11-09
Category:  Stages M2
lang: fr, en

## Responsable de stage :

Emmanuel Cosme, Julien Le Sommer, Vadim Bertrand

## Contact:

Emmanuel.Cosme@univ-grenoble-alpes.fr

## Lab:

Institut des Géosciences de l’Environnement, Grenoble

## Date

1er février - 31 juillet, 6 mois

## Mots clés : 

SWOT, dynamique océanique, JAX

## Contexte :

La mission spatiale SWOT (Surface Water and Ocean Topography, Morrow et al, 2019) a été lancée fin 2022 pour observer la dynamique océanique de très fine échelle qui se développe à la surface de l’océan. Cette dynamique turbulente de très fine échelle (quelques kilomètres) joue un rôle important dans les échanges (d’énergie et/ou de matière biogéochimique) entre la surface de l’océan et l’atmosphère et l’océan profond. Elle est donc un maillon important à comprendre pour prévoir son futur dans le contexte du changement climatique.
SWOT fournit depuis mi-2023 des images de l’anomalie du niveau de la mer à une résolution de pixel de 2 km. Malgré leur qualité inattendue, les images SWOT sont sujettes à la présence de bruit qu’il est nécessaire d’éliminer pour calculer et étudier les principales variables océaniques que sont le courant et la vorticité géostrophiques. Ces variables sont obtenues par les dérivées spatiales de l’anomalie de niveau de la mer, et l’opération de dérivation est très sensible à la présence de bruit. Au cours de la préparation de la mission et avant son lancement, l’IGE a développé un logiciel de débruitage (Gomez Navarro et al, 2020 ; site github) qui s’est d’abord révélé utile comme référence pour le développement d’autres logiciels, notamment un basé sur un réseau de neurones (Treboutte et al, 2023). Malgré tout, il semble que les résultats de ces outils ne sont pas au niveau espéré, et il est nécessaire de revisiter ce sujet.

## Sujet :

Le stage a pour premier objectif de revisiter les techniques de débruitage et d’en explorer de nouvelles, en se basant notamment sur la dynamique océanique décrite dans les équations physiques telles que la quasi-géostrophie, les écoulements peu profond, ou bien la cyclogéostrophie. Certaines méthodes de débruitage sont ou doivent être formulées de façon variationnelle. Pour faciliter au maximum l’exploration de nouvelles méthodes, il est nécessaire d’adopter une approche par modélisation différentiable. A l’IGE, nous utilisons par exemple la librairie JAX développée par Google (Bradbury et al, 2018). Une première étape du stage sera donc de moderniser le logiciel de l’IGE avec JAX, ce qui permettra une bonne familiarisation avec le sujet.

Dans une seconde étape, nous utiliserons le logiciel précédent pour traiter un échantillon de données SWOT afin de documenter la turbulence qui se développe à la surface de l’océan. La ou les régions d’intérêt, les périodes, et les variables à étudier spécifiquement seront définies collectivement. Ce travail s’appuiera en grande partie sur des bibliothèques logicielles déjà développées, à l’IGE ou par d’autres membres de l’équipe scientifique SWOT.


## Prérequis : 

Prérequis : Connaissance en dynamique des fluides géophysiques, calcul différentiel, petite expérience en programmation en python.

## References

Bradbury et al.  (2018). JAX: composable transformations of Python+NumPy programs [Computer software]. http://github.com/google/jaxn 

Gómez-Navarro et al. (2020). Development of an Image De-Noising Method in Preparation for the Surface Water and Ocean Topography Satellite Mission. https://doi.org/10.3390/rs12040734 

Morrow et al. (2019). Global Observations of Fine-Scale Ocean Surface Topography With the Surface Water and Ocean Topography (SWOT) Mission. https://doi.org/10.3389/fmars.2019.00232 

Tréboutte et al. (2023). KaRIn Noise Reduction Using a Convolutional Neural Network for the SWOT Ocean Products. https://doi.org/10.3390/rs15082183 


Image : (source : http://dx.doi.org/10.3390/rs15163939 )


![Image]({static}../figures/swot.png)




