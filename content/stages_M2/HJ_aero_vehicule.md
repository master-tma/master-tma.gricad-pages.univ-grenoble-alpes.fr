Title: Étude en soufflerie de l’aérodynamique du sillage des véhicules routiers  sous condition climatique (pluie).
Date: 2024-10-25
Category:  Stages M2
lang: fr, en

## Responsable de stage :

Henda DJERIDI et Bertrand MERCIER

## Contact:

henda.djeridi@legi.grenoble-inp.fr

## Lab:

LEGI Grenoble

## Date

6 mois, début en février 2025

## Mots clés : 

turbulence, contrôle, écoulement diphasique et transport de particules

## Contexte :

stage de M2 dans le cadre du projet ANR TWIN en collaboration avec l’école centrale de Lille et l’université d'Orléans

## Sujet :

Au cours des dernières décennies, la communauté scientifique a déployé de nombreux efforts pour étudier et contrôler l'aérodynamique complexe des véhicules routiers simplifiés à l'échelle du laboratoire, dans le but de réduire la traînée et donc la consommation de carburant dans le secteur de la mobilité des marchandises et des passagers. Cependant, le rôle des conditions extérieures reste largement inexploré. Des études ont souligné que le coefficient de traînée et la dynamique du sillage à basse fréquence sont très sensibles aux propriétés turbulentes de l'écoulement entrant. De plus, très peu d'études ont été consacrées à l'aérodynamique des véhicules routiers sous une pluie battante. Plus généralement, très peu d'attention a été accordée au couplage des particules inertielles avec l'aérodynamique.

## Prérequis : 

Mécanique des fluides, aérodynamique et turbulence

## References

[1] Passaggia et al., 2021 

[2] Smith et al., 2021 

[3] Mazellier & Obligado, 2023

![Image]({static}../figures/Graphique_sillage.png)
Figure 1: sillage autour d'un véhicule

![Image]({static}../figures/Graphique_aero.png)
Figure 2: modèle de véhicule dans la section d'essai d'une soufflerie


