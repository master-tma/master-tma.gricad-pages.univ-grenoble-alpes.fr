Title: Profilage de la couche limite turbulente par sonde pitot 3D Cobra sous le maximum du jet dans le vent catabatique sur pente forte alpine dans la vallée de Innsbruck, Autriche, dans le cadre du projet TeamX.
Date: 2023-10-06
Category:  Stages M2
lang: fr, en

## Responsable de stage :

Christophe Brun

## Contact:

christophe.brun@univ-grenoble-alpes.fr

## Lab:

LEGI Grenoble

## Date

5 mois, début en février 2024

## Mots clés : 

couche limite atmosphérique stable sur pente forte, flux turbulent de quantité de mouvement, vent catabatique, mesures in situ, paramétrisation de la turbulence, données à haute résolution spatiale et temporelle. Modélisation de la couche limite turbulente stratifiée. Effets de flottabilité et effets de gravité.

## Contexte :
La compréhension et la modélisation des écoulements atmosphériques sur relief complexe constituent des problématiques majeures pour l’amélioration des prévisions météorologiques à courts et moyens termes. La dynamique de ces écoulements est souvent à l’origine de pics de pollution dans les vallées urbanisées, en particulier en situation atmosphérique stable. Les vents catabatiques sont des écoulements gravitaires générés la nuit par le refroidissement radiatif en surface terrestre. Ils sont principalement observés en hiver lors d'épisodes météorologiques anticycloniques associés à une stratification et à une inversion de température dans la basse troposphère. Il existe un grand nombre d'observations in situ du processus catabatique le long de pentes douces (de 1° à 10°) telles que celles rapportées dans les vallées ou les glaciers, mais beaucoup moins le long de pentes alpines fortes (autour de 30°) dans les montagnes. Le vent catabatique se compose d'un jet de paroi turbulent le long de la pente couplé à une couche limite thermique turbulente refroidie, tous deux soumis aux effets de la gravité. Une approche classique du couplage est donnée par le modèle de Prandtl (1942), qui inclut également les effets turbulents, mais on sait peu de choses sur la région d'écoulement très proche en dessous de la vitesse maximale du jet pour les configurations à forte pente. Des résultats préliminaires obtenus dans le cadre d’un projet LEFE IMAGO 2021, lors d'une campagne récente de mesures in situ menée pendant l'hiver 2023 dans les Alpes françaises près de Grenoble, ont permis d'évaluer avec précision les propriétés de la couche limite turbulente dans la région située en dessous du maximum du jet catabatique, en relation avec les effets de la gravité. L'objectif de l’étude est de transposer cette campagne préliminaire à une campagne d’observation plus complète prévue dans le cadre du projet alpin européen TeamX en 2024-2026 dans les alpes autrichiennes près d’Innsbruck [0]. 

## Sujet :

L’objectif du stage est de participer à une campagne de mesure de turbulence dans les vents catabatiques sur pente alpine dans la vallée d’Innsbruck, Autriche (2 semaines en février 2024), d’analyser le jeu de données en terme de climatologie en identifiant en quoi l’épisode de mesures est caractéristique d’une situation anticyclonique stable, et de comparer les premiers résultats de mesures de profils de vitesse moyenne et de flux turbulents aux lois analytiques attendues dans ce contexte en zone très proche de la surface. Une partie du stage sera également dédiée à la mise en place d’une base de donnée de travail sur serveur OPENDAP afin de favoriser les échanges scientifiques avec l’équipe partenaire de physique atmosphérique d’Innsbruck. Si le temps le permet, les résultats de la campagne de mesure d’Innsbruck seront comparés à ceux plus établis des 4 campagnes de mesures de vents catabatiques effectuées dans le massif de Belledonne en novembre 2012 [5], avril 2015 (non publié), février 2019 [2,3,4] et février 2023 [1].

## Prérequis : 

Des compétences en turbulence, dynamique des fluides géophysiques, analyse des données sont requises. 

## References

[0] TeamX programme européen d'observation alpine 2024-2025 http://www.teamx-programme.org/)

[1] C. Brun, H. Michalet, M. Obligado, M. Lagauzere, EUROMECH Colloquium 608: Dynamics of
Gravity currents, Grenoble, 28 juin 2023.

[2] C. Brun et al., [Data set]. Zenodo. https://doi.org/10.5281/zenodo.6546702, 2022 

[3] C. Charrondière, C. Brun et al., Boundary-Layer Meteorology, 187:29–54, 2022 

[4] C. Charrondière, C. Brun, et al., Journal of Fluid Mechanics, 941, 2022

[5] C. Brun, C. Charrondière, M. Obligado, E.J. Hopfinger, J.E. Sicart, J.M. Cohard., ‘turbulent flow in the inner layer of a katabatic jet along a steep alpine slope ’. European Geoscience Union, Wien, 2022.

![Image]({static}../figures/cocainn.png)

Mat instrumenté d’un profileur pitot 3D pendant la campagne de mesure de février 2023 sur les pentes du massif de Belledonne (gauche). Mat instrumenté de l’observatoire NF27 dans la vallée d’Innsbruck (droite).
