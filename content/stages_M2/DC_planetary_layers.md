Title: Simulating the role of mountains in planetary fluid layers
Date: 2023-11-21
Category:  Stages M2
lang: fr, en

## Supervisors:

David Cébron, Dominique Jault & Paolo Personnettaz

## Contact:

david.cebron@univ-grenoble-alpes.fr

dominique.jault@univ-grenoble-alpes.fr

paolo.personnettaz@univ-grenoble-alpes.fr

## Lab:

ISTerre, Grenoble

## Date:

2024, 5-6 months 

## Context:

While the couplings between fluid and solid domains have been widely studied, their estimation remains challenging for deep planetary fluid layers with buoyancy, magnetic field, and topographic effects. Results from atmospheric or oceanic sciences are unsuitable for thick layers such as subsurface oceans of icy moons, or liquid cores of planets. Rapid ro- tation and/or the presence of a magnetic field in these regions may also cause difficulties.

## Internship:

Considering a rotating and stratified planetary fluid layer, we have developed the asymptotic local code ToCCo to investigate the small-scale topographic fluid-solid coupling due to pressure or magnetic stresses (Monville, Cébron, Jault, subm.). Using the in-house MHD code XSHELLS, this project aims at extrapolating the local model approach to the global spherical geometry. Either by perturbing XSHELLS boundary conditions (already im- plemented), or by coupling the local and the global codes (to do). Using these approaches, a self-consistent estimate of the flow (and stress) will be provided by the two-way coupling between the mountain small-scale effect and the global flow at the planetary spherical scale.

##  Funding and collaboration: 

ERC THEIA – European Research Council (ERC) under the European Union’s Horizon 2020 research and innovation programme (grant agreement no. 847433).
This research topic is directly related with the general topic "topo- graphic coupling" studied within our Geodynamo group. The student, with skills in fluid mechanics or geophysics, will thus naturally collaborate with other members of the team like R. Monville (main developer of ToCCo ) or N. Schaeffer (main developer of XSHELLS).

![Image]({static}../figures/laminar_planetary.png)

Figure 1:  Laminar flow (before the onset of turbulent flows). Copyright: R. Monville.

![Image]({static}../figures/xshells_code.png)

Figure 2:  The XSHELLS code (frontpage of the site: https://nschaeff.bitbucket.io/xshells/)
