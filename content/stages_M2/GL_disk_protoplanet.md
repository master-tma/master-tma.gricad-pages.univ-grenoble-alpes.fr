Title: Reconnexion magnétique dans les disques protoplanétaires
Date: 2023-10-24
Category:  Stages M2
lang: fr, en

## Responsable de stage :

Geoffroy Lesur

## Contact : 

geoffroy.lesur@univ-grenoble-alpes.fr

## Lab: 

IPAG Grenoble

## Date: 

5 mois, début février 2024

## Mots clés:

Plasmas astrophysiques, modélisation numérique, calcul haute performance, reconnection


## Contexte :

Les disques protoplanétaires se forment autour des étoiles jeunes et sont le lieu dans lequel se
forment les planètes. On pense que ces disques sont faiblement ionisés car il sont très froid ($$\lt 300K$$)
et denses. Néanmoins, plusieurs éléments démontrent que ces disques sont couplés au champ magnétique
dans lequel l’étoile jeune s’est formée.
Aujourd’hui, tous les modèles d’interaction entre le champ magnétique et le gaz du disque dans ces objets repose sur la magnétohydrodynamique non-idéale mono fluide (i.e. un seul fluide permet de décrire les molécules neutres majoritaires et les ions-électrons). Pourtant, il est clair que la vitesse de dérive entre ions et neutres est grande, potentiellement supersonique, en particulier dans les sites de reconnexion magnétique, ce qui rend probablement cette approximation caduc.

## Sujet :

L’objectif du stage est d’étudier un cas simplifié de reconnexion magnétique (type couche de Harris) dans un plasma peu ionisé, en comparant une approche mono fluide et une approche multi-fluide ou les molécules neutres et ions+électrons sont traitées indépendamment. On cherchera en particulier à mettre en évidence des micro-instabilités quand les vitesses de dérive deviennent importantes (i.e soniques).

Au cours de ce stage, vous apprendrez à construire un modèle numérique dans un code magnétohydrodynamique dédié au calcul haute performance écrit en C++. Vous étudierez également les instabilités dans ce type de système par une approche analytique. Un fort intérêt pour la simulation numérique et la modélisation des plasmas est souhaitable.

## Prérequis : 

Notion de programmation en C ou C++. Physique des plasmas.
Possibilité de poursuivre en thèse: oui, mais pas de financement acquis.
