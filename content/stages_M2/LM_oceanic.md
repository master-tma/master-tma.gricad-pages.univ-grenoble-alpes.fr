Title: Energetics of Jets in Two-Dimensional Oceanic Turbulence.
Date: 2023-10-20
Category:  Stages M2
lang: fr, en

## Supervisor :

Lennard Miller

## Contact :

lennard.miller@univ-grenoble-alpes.fr

## Lab :

IGE, Grenoble

## Date

2024, 5-6 months 

## Keywords : 

Theoretical Oceanography, Turbulence, Spectral Analysis

## Context :

The ocean is one of the most turbulent environments of our planet (Re ≈ 1010). At large horizontal scales, the turbulence is constrained by the ocean’s very thin aspect ratio, meaning that it can be considered two-dimensional. This yields interesting behavior: instead of the direct energy cascade of three-dimensional turbulence, two-dimensional flows are subject to an inverse cascade of energy and tend to accumulate energy at large scales. To date it remains unclear what halts this upscale transfer of energy in the ocean. On the one hand, dissipation in boundary currents such as the Gulf Stream can play a role, while another mechanism which becomes important at large scales is the excitation of planetary waves. These waves can transfer their energy into the time-mean flow and drive jets, as recently observed in the world’s ocean [1] (see figure).

## Internship :

Recent advances in idealized modeling of oceanic turbulence showed that dissipation at the coasts can be strong enough to halt the inverse cascade, producing small-scale features through fragmentation of vortices [2]. However, planetary wave activity and weak jet structures were also observed during these simulations. The goals of this internship are to characterize these structures and to see whether the model is capable of producing more pronounced jets. The student will perform high-resolution simulations of two-dimensional oceanic circulation, varying the rotation rate of the planet and analyzing the time-mean flow structure. Thereafter we will perform a flow decomposition into the time-mean flow, planetary waves and turbulence, in order to trace the pathways of energy in the flow. During the internship the student will learn how to perform computations on supercomputer clusters and acquire an advanced understanding of spectral analysis, asymptotic expansions and oceanic turbulence.

## Prérequis :

Prior knowledge of oceanic circulation theory is helpful but not required, instead we encourage students with backgrounds in physics or mathematics to apply.

## References:

[1] N. A. Maximenko, B. Bang, and H. Sasaki. Observational evidence of alternating zonal jets in the world ocean. Geophysical research letters, 32(12), 2005.
[2] L. Miller, A. Venaille, and B. Deremble. Gyre turbulence (arxiv.org/abs/2310.02187), 2023.


![Image]({static}../figures/oceanic_jet.png)

Figure 1: Ocean surface velocity along westwards direction from [1]. The jets are the alternating elongated structures in the green box.
