Title: Transition à la turbulence quantique en rotation
Date: 2023-10-27
Category:  Stages M2
lang: fr, en

## Responsable de stage :

Mathieu Gibert (Chercheur CNRS)

## Contact : 

Mathieu.gibert@neel.cnrs.fr

## Lab: 

Institut Néel – département MCBT – Équipe HELFA https://neel.cnrs.fr/equipes-poles-et-services/helium-du-fondamental-aux-applications-helfa

## Date: 

5 mois, début février 2024

## Mots clés:

Experimental rotating superfluid turbulence

## Contexte :

Les vortex quantiques dans l’hélium II constituent la brique élémentaire de tous les écoulements superfluides. Notre projet est de les étudier par visualisation directe. A l’Institut Néel, nous avons développé le seul cryostat au monde aujourd’hui capable de décorer ces vortex de particules microniques de dihydrogène solide dans des conditions expérimentales contrôlées et reproductibles. En utilisant 3 caméras rapides et une caméra sensible haute résolution, nous sommes capables de reconstruire les trajectoires de ces flocons de dihydrogène piégés sur le cœur des vortex. De plus, le cryostat et l’instrumentation sont montés sur une table tournante, ce qui permet la génération et l’étude de réseaux de vortex alignés avec l’axe de rotation. Cet état canonique (comparable avec un réseau d’Abrikosov en supraconductivité) sera utilisé comme configuration de base dans les écoulements que nous considérerons dans ce stage.

## Sujet :

En perturbant ce réseau stationnaire, par un contre-écoulement, nous avons mis en évidence plusieurs régimes. Un premier (A), ou le réseau reste insensible à la perturbation, un second (B) ou des ondes se propagent sur les vortex quantiques et un troisième (C) qui est un régime turbulent. Ces différents régimes définissent une voie vers la turbulence quantique en rotation. L’ambition du stage sera de caractériser expérimentalement la transition entre le régime ondulatoire (B) et pleinement turbulent (C).

## Prérequis : 

Instrumentation en mécanique des fluides, turbulence, analyse d’image, thermodynamique, cryogénie.

## References

https://www.science.org/doi/full/10.1126/sciadv.adh2899

Direct visualization of the quantum vortex lattice structure, oscillations, 
and destabilization in rotating 4He.
Charles Peretti, Jérémy Vessaire, Emeric Durozoy, and Mathieu Gibert, Sci. Adv. 9, eadh2899 (2023)

![Image]({static}../figures/quantum_vortex.png)
