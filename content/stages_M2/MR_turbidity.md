Title: To what extent local particle/fluid physical processes drive turbidity currents global dynamics?
Date: 2023-10-05
Category:  Stages M2
lang: fr, en

## Supervisor :

Marie Rastello & Florence Naaim

## Contact :

marie.rastello@cnrs.fr

## Lab :

LEGI & IGE, Grenoble

## Date

2024, 5-6 months 

## Keywords : 

experimental ; instrumentation ; turbulent flow ; high Reynolds

## Context :

The M2 internship is in connection with ANR project PALAGRAM (LEGI/ INRAE(ETNA)/ IMFT/ LEMTA) (2020-2024) that is dedicated to the interactions between particles and fluid within turbidity currents and their impact on the overall flow behavior. Turbidity currents are self-sustained flowing particle suspensions. Many geophysical flows involve them: pyroclastic flows, sandstorms, snow avalanches, underwater turbidity currents, blowing snow in katabatic winds... Given their humongous strength they are still nowadays synonyms of very high costs when it comes to infrastructures or lives.. Despite the scaling effect studying turbidity currents physical properties in the lab is an interesting key to handle more accurately natural flows. In this context, the main objective of this internship is to bring in accurate information on the impact of particle/turbulence interaction on the overall finite size turbidity currents (FSTC) behavior.

## Internship :

The student will tackle carefully laboratory experiments on FSTC using PIV (Particle Image Velocimetry). We will deal with a large focus on “microscopic” processes. The interaction between particles and turbulence within the flow will be looked through. The influence of Stokes number, particle Reynolds number and density ratio on the physics of the phenomena will be investigated thoroughly as the flows are expected to be a mix of granular/suspension/turbulent flows. This work will be done in connection with large eddy simulations of the same type of flows that are carried out at LEGI within the PALAGRAM project context. The whole frame will allow to build a diagram connecting micro and macro physical phenomena through retroactive loops. The influence of the granular phase versus the suspended one within the turbulent dynamics of the overall flow will be quantified as it appears to be one of the still missing keys of the whole understanding of the FSTC behaviors.
Optically indexed matched spheres with the carrying fluid will be used. We will leverage on the transparency in water of the spheres to monitor both fluid and particles velocities at the same location and time. All the data will be acquired through image processing and time resolved PIV. A post-processing in Matlab or Python will be done.

A possibility of pursuing with a phd after the M2 can be considered.

## References

![Image]({static}../figures/pyroclastic.png)
Figure 1: pyroclastic flows

![Image]({static}../figures/avalanche.png)
Figure 2: power snow avalanche

![Image]({static}../figures/turbid.png)
Figure 3 : turbidity cloud
