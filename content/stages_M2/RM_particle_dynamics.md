Title: Particle dynamics in turbulent flows
Date: 2023-11-21
Category:  Stages M2
lang: fr, en

## Supervisor :

Romain Monchaux

## Contact :

romain.monchaux@ensta-paris.fr

## Lab :

ENSTA-Paris, Institut Polytechnique de Paris, Palaiseau

## Date

2024, 5-6 months 

## Keywords : 

experimental fluid mechanics, Two phases flows, Turbulence

## Context :

How do rain droplets form in clouds? How pollutants, viruses or volcano ashes disperse in the atmosphere? Where can they fall and at which speed? How plankton dynamics is affected by sea conditions? What are the consequences on the trophic chain? What are the optimal parameters for a wastewater treatment plant, for an engine, a chemical reactor? All these questions find answers in the study of inertial particles in turbulence. Such particle laden flows are ubiquitous but so complex to study due to the very large number of intertwined control parameters and the lack of theoretical predictions.

Our group has a longstanding experience in the study of these flows for which we have developed specific analysis tools based on coupled experimental measurement of both phases (fluid and particles) and on efficient post-processing methods. We aim at pursuing our efforts on the understanding of preferential concentration and clustering of inertial particles and to get new insights in the coupling between phases through energy and momentum exchange quantification from cutting edge 4D PIV/PTV system. Collaborations with CIEMAT (Madrid, Spain), LEGI (Grenoble, France) and CMAP (Polytechnique, Palaiseau, France) are developed to perform simultaneously numerical simulations or complementary experiments.

## Internship :

Master internship position is open to study both the couplings between particle and turbulent dynamics. We propose experimental works on the dynamics of microparticles in large scale turbulent or shear flows. We perform Eulerian measurements of the carrier fluid (turbulence) and Lagrangian measurements to resolve the dynamics of the particles and to understand the couplings between the two. Very few teams in the world are able to perform both measurements simultaneously, which puts us in an interesting position to tackle this issue experimentally. Possibilty of teaching in top ranking French “Grande Ecole”.

## Prérequis :

Strong background in experimental fluid mechanics.
Application must include CV (2 pages max), motivation letter (1 page), BSc/MSc transcripts and three names for references.


![Image]({static}../figures/particule_clouds.png)

Figure 1: Particle clouds aplications
