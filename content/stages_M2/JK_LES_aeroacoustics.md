Title: Modélisation aéroacoustique autour d'un prototype d'automobile à partir de simulation numérique LES
Date: 2023-01-14
Category:  Stages M2
lang: fr, en

## Supervisor :

Johannes Kreuzinger & Florian Schwertfirm

## Contact :

j.kreuzinger@km-turbulenz.de

f.schwertfirm@km-turbulenz.de

## Company:

KM-Turbulenz GmbH, Munich, Germany

## Date

5 months, starting from March 2023

## Keywords : 

Aeroacoustic Simulation, Large Eddy Simulation, Acoustic Perturbation Equation, Analysis of acoustic fields, Automotive

## Context :

The main product of KM-Turbulenz is an CFD-CAA tool which couples incompressible large eddy simulation with acoustic perturbation equations to compute the sound field induced by low Mach number flows [1]. It is mainly applied in German automotive industry. To judge future developments of the simulation software, a test case for external flow whose sound radiation is analysed in a very detailed way will help.

## Internship :

The first step of the internship will be to conduct an aeroacoustic simulation of the flow around a prototypical car (DrivAer body) in an numerical wind tunnel following best practice guidelines (as an example see [2]). From this simulation time resolved data in the near an far field can be stored. Besides the standard postprocesssing (computation of spectra, power of radiated sound and visualisations) advanced evaluations should be performed, namely beam forming and wave number frequency spectra. This will be done using either in-house tools or open source packages. Interfaces to the evaluation tools will have to be programmed. As result there will be a deeply analysed reference case, which will be useful in checking the quality of further developments of the simulation software.
Expected knowledge: CFD, Basics of scripting and python programming

## References

[1] Schwertfirm, F., Kreuzinger, J., Peller, N. and Hartmann, M.: Validation of a Hybrid Simulation Method for Flow Noise Prediction, AIAA/CEAS Aeroacoustic Conference, Colorado Springs, 2012
[2] Florian Schwertfirm, Michael Hartmann: Acoustic-Fluid-Structure Interaction (AFSI) in the car underbody, ISNVH 2022, Graz

![Image]({static}../figures/car_aeroacoustix2.png)
Figure 1: LES of aeroacoustic fields around a car model with MGLET flow and aeroacoustic solver developed by KM-Turbulenz. Results using robust numerics and efficient Cartesian grids with immersed boundary methods.
