Title: Approche multi-fidélité pour les écoulements turbulents
Date: 2023-10-20
Category:  Stages M2
lang: fr, en

## Responsable de stage :

 Thomas Berthelon & Guillaume Balarac

## Contact:

 thomas.berthelon@univ-grenoble-alpes.fr, guillaume.balarac@univ-grenoble-alpes.fr

## Lab:

LEGI Grenoble

## Date

5 mois, début en février 2024

## Mots clés : 

Large Eddy Simulation, Multi-fidélité, Machine Learning, Turbulence

## Contexte :
La forte augmentation de la puissance de calcul observée au cours de ces dernières années a permis d’utiliser les Simulations aux Grandes Échelles (ou Large Eddy Simulations) pour des configurations industrielles. Cependant, le temps de restitution est trop important pour évaluer un grand nombre configurations. La LES reste donc confinée à une utilisation occasionnelle sur certains points de fonctionnement.

## Sujet :

Afin d’élargir son utilisation, une approche multi-fidélité est envisagée dans ce stage. Les méthodes de multi-fidélité consistent à compléter les résultats précis mais peu nombreux provenant d’une méthode haute- fidélité avec des résultats moins précis provenant d’une méthode basse-fidélité mais qui couvre un espace des paramètres plus étendu. Les premiers travaux sur ce type d’approche ont été réalisés en 2000 par Kennedy et al [1]. Aujourd’hui, avec l’essor de l’apprentissage automatique, ces thématiques regagnent de l’intérêt [2], y compris dans la communauté de la mécanique des fluides numérique [3].
Le stage débutera par une phase primordiale de bibliographie sur les méthodes mutli-fidélité. Ensuite, des approches classiques de multi-fidélité seront développées en python à l’aide de la librairie sklearn. Ces méthodes seront évaluées sur des cas analytiques simples. Des études paramétriques permettront de mettre en avant les atouts et les faiblesses des différentes méthodes. Dans un second temps, ces méthodes seront appliquées sur des cas de mécanique des fluides. Des calculs LES seront effectués à l’aide du code YALES2 afin de générer des données haute et basse fidélité, notamment en évaluant différents maillages grâce à de récents travaux sur l’adaptation de maillage [4].

## Prérequis : 

Connaissances attendues : Modélisation numérique, Mécanique des fluides, Python

## References
 
[1] Marc C Kennedy and Anthony O’Hagan. Predicting the output from a complex computer code when fast approximations are available. Biometrika, 87(1) :1–13, 2000.

[2] M Giselle Fernández-Godino, Chanyoung Park, Nam-Ho Kim, and Raphael T Haftka. Review of multi-fidelity models. arXiv preprint arXiv :1609.07196, 2016.

[3] Andrew Mole, Alex Skillen, and Alistair Revell. Multi-fidelity surrogate modelling of wall mounted cubes. Flow, Turbulence and Combustion, 110(4) :835–853, 2023.

[4] A Grenouilloux, J Leparoux, V Moureau, G Balarac, T Berthelon, R Mercier, M Bernard, P Bénard, G Lartigue, and O Métais. Toward the use of les for industrial complex geometries. part i : automatic mesh definition. Journal of Turbulence, pages 1–31, 2023.

![Image]({static}../figures/methode_multi_fidelite.png)

Illustration d’une méthode multi-fidélité
