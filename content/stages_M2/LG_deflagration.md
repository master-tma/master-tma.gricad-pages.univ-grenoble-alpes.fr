Title: Comparaison des approches statistiques et de la simulation des grandes échelles pour la déflagration.
Date: 2023-11-22
Category:  Stages M2
lang: fr, en

## Responsables de stage :

Laura Gastaldo & Dorian Trabichet

## Contact:

laura.gastaldo@irsn.fr

dorian.trabichet@irsn.fr

## Laboratoire :

IRSN Cadarache

## Dates :

4-6 mois, début en avril 2024

## Mots clés : 

Explosion de gaz – Modélisation – Simulation des grandes échelles - Calcul scientifique

## Contexte :

Pour l’évaluation des risques d’explosion d’hydrogène, et, plus généralement, de combustibles gazeux, l'IRSN développe le logiciel CFD open-source CALIF3S. Cet outil de calcul traite les déflagrations turbulentes. Deux approches sont disponibles. La première est basée sur une modélisation de la turbulence statistique (RANS) ; le modèle de combustion utilise une approche de type level-set, et est fermé par une corrélation de vitesse de flamme dépendant des caractéristiques turbulentes de l'écoulement. La deuxième approche est basée sur une modélisation de la turbulence par Simulation des Grandes Echelles (LES) et un modèle de flamme épaissie dynamique (TFLES) pour traiter la combustion.

## Sujet :

L’objet de ce stage, d’une durée minimale de 4 mois, est d’effectuer des comparaisons entre ces deux approches sur un cas de déflagration accélérée par des obstacles [1]. Le stage débutera par une prise en main du code de calcul et une familiarisation aux approches RANS et LES pour la déflagration. Les résultats seront, dans un premier temps, comparés aux données expérimentales disponibles. Il s’agira ensuite d’effectuer une analyse critique de l’approche RANS en analysant de façon détaillée les résultats obtenus. Il s’agira notamment d’extraire les données statistiques des écoulements transitoires obtenus par simulation LES afin d’estimer l’énergie cinétique turbulente de l’écoulement, essentielle au bon fonctionnement du modèle de combustions RANS. Plusieurs techniques de prise de moyenne seront envisagées. Les données extraites seront confrontées à celles obtenues à partir des méthodes RANS.

## Prérequis : 

Des compétences en turbulence, dynamique des fluides géophysiques, analyse des données sont requises. 

## References

[1] JOHANSEN, Craig T. et CICCARELLI, Gaby. Visualization of the unburned gas flow field ahead of an accelerating flame in an obstructed square channel. Combustion and Flame, 2009, vol. 156, no 2, p. 405-416.
