Title: Transition to rotating quantum turbulence
Date: 2023-10-27
Category:  Stages M2
lang: fr, en

## Responsable de stage :

Mathieu Gibert (Chercheur CNRS)

## Contact : 

Mathieu.gibert@neel.cnrs.fr

## Lab: 

Institut Néel – département MCBT – Équipe HELFA https://neel.cnrs.fr/equipes-poles-et-services/helium-du-fondamental-aux-applications-helfa

## Date: 

5 mois, début février 2024

## Mots clés:

Experimental rotating superfluid turbulence

## Contexte :

Quantum vortices in helium II are the building blocks of all superfluid flows. Our project is to study them using direct visualization. At the Institut Néel, we have developed the only cryostat in the world today capable of decorating these vortices with micron-sized particles of solid dihydrogen under controlled and reproducible experimental conditions. Using 3 high-speed cameras and a sensitive high-resolution camera, we can reconstruct the trajectories of these dihydrogen flakes trapped at the heart of the vortices. In addition, the cryostat and instrumentation are mounted on a turntable, enabling the generation and study of vortex arrays aligned with the axis of rotation. This canonical state (comparable with an Abrikosov lattice in superconductivity) will be used as the basic configuration in the flows we will consider in this internship.

## Sujet :

By perturbing this stationary network with a counter-flow, we've demonstrated several regimes. The first (A), where the network remains insensitive to the perturbation; the second (B), where waves propagate on the quantum vortices, and the third (C), is a turbulent regime. These different regimes define a path towards rotational quantum turbulence. The internship aims to experimentally characterize the transition between the wave regime (B) and the fully turbulent regime (C).

## Prérequis : 

Instrumentation in fluid mechanics, turbulence, image analysis, thermodynamics, cryogenics.

## References

https://www.science.org/doi/full/10.1126/sciadv.adh2899

Direct visualization of the quantum vortex lattice structure, oscillations, 
and destabilization in rotating 4He.
Charles Peretti, Jérémy Vessaire, Emeric Durozoy, and Mathieu Gibert, Sci. Adv. 9, eadh2899 (2023)

![Image]({static}../figures/quantum_vortex.png)
