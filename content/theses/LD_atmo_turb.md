Title: Multi-scale interactions in the atmosphere. Application to the prediction of heat waves and urban heat islands.
Date: 2023-05-10
Category:  Theses
lang: fr, en

## Supervisor :

Luminita Danaila, K.P. Chun  and M. Ghil

## Contact :

luminita.danaila@univ-rouen.fr

## Lab :

Laboratoire Morphodynamique Continentale et Côtière M2C, Rouen
West England University at Bristol, UK
ENS, Paris

## Date

2023-2026

## Keywords : 



## Context :

The climate system is described by approaches focusing on Zonal jets, Waves, and Eddies, hypothesizing that they act at separate space/time scales. However, recent studies have shown that, for the atmosphere and oceans, wavelike features interact with turbulent cascades and give rise to distinct regimes that are crucial for mixing and dissipation. In these planetary fluids, smaller-scale turbulent statistics correlate with the same quantity's long-time and large-scale field. For example, daily, seasonal, and even interdecadal phenomena affect local, strong temperature fluctuations.

## PhD:

The thesis aims to provide physical arguments for this conditioning of the smaller scales by the largest ones. Turbulent statistics will be computed over each range of scales for several phases of the large-scale, long-time phenomena. The methodology combines simulations performed with WRF (Weather Research and Forecasting) code developed at Boulder, Colorado, as well as ERA5 data, with theoretical developments of transport equations for higher-order statistics of velocity, temperature, humidity, and precipitations. This methodology can thus be applied to improve extreme events modeling to predict heavy rainfall, drought, storms, and floods.

## References



 
