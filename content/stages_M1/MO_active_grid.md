Title: The turbulence properties of active-grid-generated flows
Date: 2022-02-12
Category:  Stages M1
lang: fr, en

## Supervisor :
Martin Obligado
## Contact :
Martin.Obligado@univ-grenoble-alpes.fr
## Lab :
LEGI Grenoble
## Date :
Spring of 2022
## Key Words :
Experimental fluid mechanics, turbulence, hot-wire anemometry
## Context :
Active-grid-generated turbulence has become a standard way to generate moderate-to-high Reynolds numbers in wind/water tunnels. These grids, composed by several blades and motors than can be controlled independently, can be used in two-phase conditions, and to generate atmospheric and other bespoke flows in the wind tunnel. It is therefore an optimal tool to also study the properties of the energy cascade of turbulence, as it can directly be related to the dynamic modes created.
## Project :
The trainee will perform experiments in LEGI wind tunnel using hot-wire anemometry and the active grid. He/she will also program the grid to generate different dynamic protocols, do the data postprocessing and interpretation of results.

![Image]({static}../figures/activ_grid.png)
