Title: Analyse de données in situ de turbulence
Date: 2022-01-31
Category:  Stages M1
lang: fr, en

## Encadrants :

Christophe Brun (équipe MEIGE)

## Contact :

christophe.brun@univ-grenoble-alpes.fr

## Laboratoire :

LEGI Grenoble

## Dates :

2 mois à partir du printemps 2022

## Mots clés :

couche limite atmosphérique, stratification thermique stable, vents catabatiques, antarctique

## Contexte :

Des mesures in situ le long d’un raid de ravitaillement sur le plateau Antarctique ont été effectuées à l'occasion de 2 séjours de 3 mois en Antarctique au cours de l’été austral 2011 puis 2013. Il s'agissait d'une collaboration à l’encadrement de la thèse d'Hélène Barral (2015) avec Christophe Genthon (IGE). Les mesures de turbulence ont été réalisées le long d'un raid de ravitaillement logistique de 10 jours entre la base cotière de Dumont D’urville et la base franco-italienne Concordia (Dôme C : altitude 3400 m) située à 1000km à l’intérieur du plateau Antarctique. Une campagne de mesure inédite de la turbulence (station méteo et anémométrie sonique 3D) a été réalisée chaque nuit au point d’arrêt du convoi. On dispose ainsi d’une base de données de 10 nuits de mesure de turbulence en couche de surface de la couche limite atmosphérique antarctique qui permet de caractériser le mélange turbulent en milieu extrème et questionner l’universalité des lois de type Monin-Obukhov et des lois de frottement vs rugosité en terrain réel antarctique.

## Sujet :

Il s'agira de réanalyser les données de vitesse 3D échantillonées à 20 Hz afin d'en extraire des informations sur la stationarité de la couche limite catabatique nocturne en antarctique, de reconstruire le tenseur de Reynolds et l'ensemble des flux de chaleur sensible. On se focalisera sur la modificaton de la couche limite turbulente en surface par les effets de flottabilité et de rugoisté de surface. On analysera également les propriétés spectrales de la turbulence en quantifiant les écarts par rapports à des couches limites turbulentes neutres classiques.

![Image]({static}../figures/antarctique4.jpg)
