Title: Etude expérimentale de l’intensification des transferts thermiques par ultrasons – analyse énergétique selon la puissance ultrasonore
Date: 2022-02-18
Category:  Stages M1
lang: fr, en

## Encadrants :

Christophe Poncet, Odin Bulliard-Sauret (équipe ENERGIE)

## Contact :

odin.bulliard-sauret@univ-grenoble-alpes.fr

## Laboratoire :

LEGI Grenoble

## Dates :

printemps 2022

## Mots clés :

Energétique / Mécanique des fluides / Transferts thermiques

## Contexte :

En convection forcée, afin d’améliorer les échanges de chaleur, la solution est souvent l’augmentation du débit, afin d’augmenter la turbulence de l’écoulement. Cela induit également une augmentation de la puissance hydraulique et donc du coût de pompage. Il est cependant possible d’intensifier les transferts grâce à d’autres procédés, tel que les ultrasons.
En effet, l’intérêt scientifique pour l’intensification des transferts thermiques par ultrasons a connu un essor notable au cours des dernières années. De nombreux articles ont permis de démontrer l’efficacité des ultrasons en convection naturelle ou forcée, ou encore dans le cas d’échangeur de chaleur amélioré par ultrasons. Les ultrasons produisent des effets différents sur l’hydrodynamique ou le transfert thermique selon la puissance ultrasonore ou les fréquences utilisées. Cependant, plusieurs aspects restent encore inexplorés sur l’intensification des transferts thermiques en convection forcée.
Le régime d’écoulement est généralement caractérisé par le nombre de Reynolds, défini comme un rapport, entre le produit de la vitesse du fluide et la dimension caractéristique de l’écoulement (dans le cas d’une conduite, c’est généralement le diamètre hydraulique), et la viscosité cinématique du fluide. De récent travaux ont été menée concernant l’influence du régime d’écoulement sur l’intensification du transfert thermique en présence d’ultrasons. Ainsi, les résultats ont montré qu’en présence d’ultrasons, l’intensification des transferts thermiques diminuait avec l’augmentation du nombre de Reynolds. Cependant les ultrasons de basse fréquence permettent de maintenir un niveau d’intensification des transferts thermiques de l’ordre de 50% même en régime turbulent, tandis que les ultrasons de haute fréquence ne produisent plus d’intensification des transferts thermiques, lorsque l’écoulement est turbulent. D’autres travaux plus anciens ont également montré que l’augmentation de la puissance ultrasonore permettait d’augmenter l’intensification des transferts thermiques, suivant une allure asymptotique. Néanmoins, aucune étude n’a été menée sur l’influence de la puissance acoustique sur l’intensification du transfert thermique générée par les ultrasons, selon le régime d’écoulement.
Ainsi, il semble intéressant d’étudier l’influence que peut la puissance acoustique selon le régime d’écoulement, et pour différentes fréquences ultrasonores. Il sera ainsi possible de mieux caractériser l’influence des ultrasons sur le transfert thermique en convection forcée, dans une perspective d’utilisation des ultrasons dans des procédés industriels.

## Sujet :

En tant que stagiaire, vous serez amené(e) à travailler avec un post-doctorant sur l’influence de la puissance acoustique selon le régime d’écoulement. Dans le cadre de ce stage, vous réaliserez des mesures thermo-hydrauliques sur un banc d’essais, actuellement opérationnel. Ce banc d’essais permet d’étudier l’influence des ultrasons sur le transfert thermique, selon différents diamètres hydrauliques et différentes vitesses d’écoulement, permettant d’obtenir une large gamme de nombre de Reynolds. Ces mesures permettront d’analyser les effets induits par les ultrasons respectivement sur le transfert thermique et l’hydrodynamique. Ces essais seront menés pour plusieurs fréquences ultrasonores.
Enfin, ces analyses permettront de dresser un bilan énergétique global de l’intérêt des ultrasons pour intensifier les transferts de chaleur, comparativement à d’autres méthodes d’amélioration des transferts thermiques.

Connaissances en mécanique des fluides et transferts thermiques. Expérience PIV appréciée.
Intérêt pour le travail expérimental et la compréhension des phénomènes physiques.
Bonne capacité de rédaction.

