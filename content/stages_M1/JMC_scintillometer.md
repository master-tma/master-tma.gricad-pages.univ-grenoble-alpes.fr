Title: Turbulence optique
Date: 2022-01-31
Category:  Stages M1
lang: fr, en

## Encadrants :
Jean-Martial Cohard
## Contact :
jean-martial.cohard@univ-grenoble-alpes.fr
## Laboratoire :
IGE Grenoble
## Dates :
2 mois à partir du printemps 2022
## Mots clés :
turbulence optique, couche limite atmosphérique, transfert surface atmosphère
## Contexte :
Turbulent exchanges from surface to atmosphere, like sensible heat flux,  remains particularly difficult to quantify, especially on complex and heterogeneous landscapes. Since the 1990s, scintillometry has been recognized as an accurate method to estimate turbulent fluxes at km² scales compatible with a satellite pixel or a hydrological model mesh. If optic scintillometry is today considered to be an accomplished technique to measure spatially integrated sensible heat fluxes, it is difficult to attribute the surface from which this energy is coming from. Specifically,  the integration of terrain complexity in footprint models is not well taken into account. A new footprint model formulation has been proposed to overcome this lack but still need to be validated. A specific field campaign has been done providing the necessary data in summer 2018.
## Sujet :
The candidate will process the data and from the field campaign to obtain integrated sensible heat fluxes from the scintillometer installed with a slanted path. This will be compared to local flux measurements from two Eddy-covariance towers settled on different land-use. The footprint model predict the partition of each land-use type. The weighted fluxes according to the footprint partition should be equal to the sensible heat flux deduced from the scintillometer observations.


![Image]({static}../figures/scintillometer.JPG)
