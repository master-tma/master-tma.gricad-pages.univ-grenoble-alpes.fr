Title: Strongly stratified turbulence
Date: 2022-03-04
Category:  Stages M1
lang: fr, en

## Supervisor :
Nicolas Mordant
## Contact :
nicolas.mordant@univ-grenoble-alpes.fr
## Lab :
LEGI Grenoble
## Date :
Spring 2022
## Key Words :
Experimental fluid mechanics, turbulence, geophysical flows, internal waves, wave turbulence

## Context :

Geophysical flows subjected to the action of the Coriolis force due to rotation of the Earth or to a stable density stratification (due to variations of temperature and salinity) support the propagation of internal waves in the interior of the fluid. When the Reynolds number is sufficiently large, turbulence develops that is made of non linearly interacting vortices and waves. At submesoscales, the oceanic turbulence is assumed to be wave turbulence of internal waves that build up the observed Garrett & Munk spectrum. In the Coriolis facility, we investigate stratified(-rotating) turbulence excited by large scale waves and compare the resulting flows to the phenomenology of wave turbulence.

## Project :

The trainee will contribute to an experimental campaign on the Coriolis facility (13m-diameter turntable, 1m deep, 130 tons of water) that will start in April 2022. He will participate in data acquisition and preliminary data processing. These results will be compared to that of previous experimental campaigns so that to investigate if a regime of strongly stratified turbulence can be reached in the experiment.

![Image]({static}../figures/21INTERNAL.jpg)
Coriolis platform in LEGI Grenoble
