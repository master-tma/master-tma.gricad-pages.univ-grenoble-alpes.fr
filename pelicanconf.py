#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = "Contact : Christophe.Brun_@_univ-grenoble-alpes.fr"
SITENAME = "Master Turbulences, Méthodes & Applications"
SITEURL = ""

PATH = "content"
OUTPUT_PATH = "public"
STATIC_PATHS = ["figures"]

TIMEZONE = "Europe/Athens"

# DEFAULT_LANG = 'en'
# DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
#FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
SHORTCUT_ICON = 'logo-uga.jpeg'
# Blogroll
#<img src="/figures/logo-uga.jpeg" alt="screenshot" style="width:50px;" />
LINKS = (
    (
        "UGA: Faculté des Sciences",
        "https://www.univ-grenoble-alpes.fr/les-composantes/faculte-des-sciences-638020.kjsp",
        #'logo-uga.jpeg'
    ),
    ("UFR PHITEM", "https://phitem.univ-grenoble-alpes.fr"),
    ("LEGI", "http://www.legi.grenoble-inp.fr/"),
    #('github', 'https://github.com/'),
    #('Pelican', 'http://getpelican.com/'),
    #         ('Python.org', 'http://python.org/'),
    #         ('Jinja2', 'http://jinja.pocoo.org/'),
)

# Social widget
#'/figures/logo-uga.jpeg'
SOCIAL = (
    ("journée des Masters UGA", "http://prose.univ-grenoble-alpes.fr",'logo-uga.jpeg'),
    ("École de Physique des Houches", "http://www.nctr.eu"),
    #("École de Physique des Houches", "http://www.nctr.eu",'github'),
    #('github', 'https://github.com/'),
)
#SIDEBAR_IMAGES_HEADER = 'My Images'
SIDEBAR_IMAGES = ["/figures/logo-uga.jpeg"]

# DEFAULT_PAGINATION = 5
DEFAULT_PAGINATION = 1

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

PLUGIN_PATHS = ["plugins"]
PLUGINS = ["i18n_subsites"]

# JINJA_EXTENSIONS = ['jinja2.ext.i18n']
JINJA_ENVIRONMENT = {"extensions": ["jinja2.ext.i18n"]}

# THEME = "themes/notmyidea2"
THEME = "themes/pelican-bootstrap3"
# THEME = 'themes/notmyidea'
# THEME = 'themes/simple'

DEFAULT_LANG = "fr"
LOCALE = "fr_FR"

I18N_SUBSITES = {
    "en": {
        "SITENAME": "Major Turbulences, Methods & Applications",
    }
}
I18N_TEMPLATES_LANG = "en"

# i18n_subsites = {
#        'en': {
#            'SITENAME': 'Major Turbulences, Methods & Applications',
#            'LOCALE': 'en_EN',            #This is somewhat redundant with DATE_FORMATS, but IMHO more convenient
#            },
#        }

DEFAULT_CATEGORY = "misc"

OUTPUT_SOURCES = True

languages_lookup = {
    "en": "English",
    "fr": "Français",
}


def lookup_lang_name(lang_code):
    return languages_lookup[lang_code]


def my_ordered_items(dict):
    items = list(dict.items())
    # swap first and last using tuple unpacking
    items[0], items[-1] = items[-1], items[0]
    return items


JINJA_FILTERS = {
    "lookup_lang_name": lookup_lang_name,
    "my_ordered_items": my_ordered_items,
}

# STATIC_PATHS = ['images', 'files']

# EXTRA_PATH_METADATA = {
#    'files/.nojekyll': {
#        'path': '.nojekyll',
#        },
#    }
DISPLAY_CATEGORIES_ON_MENU = False
BOOTSTRAP_NAVBAR_INVERSE = True
PAGES_SORT_ATTRIBUTE = 'sortorder'

# DISPLAY_BREADCRUMBS = True
# DISPLAY_CATEGORY_IN_BREADCRUMBS = True

DISPLAY_ARTICLE_INFO_ON_INDEX = False

# USE_PAGER = True
BOOTSTRAP_FLUID = True

DISPLAY_ARTICLE_INFO_ON_INDEX = True
